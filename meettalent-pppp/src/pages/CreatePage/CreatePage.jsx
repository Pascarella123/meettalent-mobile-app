import React from 'react';
import { Link } from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import { Navbar } from '../../shared/components/Navbar/Navbar';

import './CreatePage.scss';

export function CreatePage(){
   
    return (
        <>
            <CSSTransition
                appear
                in
                classNames='navbar-transition'
                timeout={800}
            >
            <>
                <div className="c-create">
                    <button className="b-round-button b-round-button--create c-createOffer__distance"><span className="icon-file-text"></span></button>
                    <Link to='/create/offers'>
                        <button className="b-button">Crear oferta</button>
                    </Link>
                    <hr/>
                    <button className="b-round-button b-round-button--create c-createOffer__distance"><span className="icon-video"></span></button>
                    <Link to='/create/hackathon'>
                        <button className="b-button">Crear prueba</button>
                    </Link>
                </div>  
                <CSSTransition
                    appear
                    in
                    classNames='navbar-transition'
                    timeout={800}
                >
                    <Navbar/>
                </CSSTransition>

            </>
            </CSSTransition>
        </>
    )
}