import { useEffect, useContext, useState } from "react";
import { useHistory } from "react-router";

import { IsLoggedContext } from '../../../../shared/contexts/IsLoggedContext';

import facebookLogo from "../../../../assets/images/facebookLogo.jpg";
import googleLogo from "../../../../assets/images/googleLogo.jpg";
import linkedInLogo from "../../../../assets/images/linkedinLogo.png";

export function OAuth({ social, socket }) {

    const [ user, setUser ] = useState({});
    const {setIsLogged} = useContext(IsLoggedContext);

    const history = useHistory();

    const popUp = () => {
        const width = 600, height = 600;
        const left = (window.innerWidth / 2) - (width / 2);
        const top = (window.innerHeight / 2) - (height / 2);

        const url = 'http://localhost:5500/auth/'+social+'?socketId='+socket.id;

        const popUpWindow = window.open(url, '',       
            `toolbar=no, location=no, directories=no, status=no, menubar=no, 
            scrollbars=no, resizable=no, copyhistory=no, width=${width}, 
            height=${height}, top=${top}, left=${left}`
        );

        socket.on('auth', userAuth => {
            setUser(userAuth); 
            
            popUpWindow.close();
        });

    }

    useEffect(() => {
        if(user.token && user.user){
            document.cookie = 'token=' + user.token;
            document.cookie = 'user=' + JSON.stringify(user.user);

            setIsLogged(true);

            history.push('/authentication/congrats'); 
        }
    },[user]);

    return(
        <button className="b-social-btn" onClick={popUp}>
            <img src={social === 'facebook' ? facebookLogo : (social === 'google' ? googleLogo : linkedInLogo)} alt={social+' logo'}/>
        </button>
    )
    
}