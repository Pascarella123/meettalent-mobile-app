import React, { useState, useContext, useEffect } from 'react';
import {Link, useHistory} from 'react-router-dom';
import { useForm } from "react-hook-form";
import {CSSTransition} from 'react-transition-group';

import { IsLoggedContext } from '../../../shared/contexts/IsLoggedContext';
import { PageContext } from '../../../shared/contexts/PageContext';
import { OAuth } from './components/OAuth'; //??
import { API } from '../../../shared/services/api';

import { PasswordEye } from '../../../shared/components/littleComponents/PasswordEye/PasswordEye';

import './RegisterPage.scss';

//En app ??
import io from 'socket.io-client'; //??

const socket = io('http://localhost:5500');  //config o .env !!!! ruta back

export function RegisterPage(){

    const [email, setEmail] = useState();

    const { register, handleSubmit, reset,  formState: { errors }  } = useForm();

    const {setIsLogged} = useContext(IsLoggedContext);
    const {page, setPage} = useContext(PageContext);

    const [errorMatch, setErrorMatch] =useState();

    const history = useHistory();

    // --- \\

    const toRegisterLocal = formData => {
        const request = {emailID: formData.emailID};
        API.post('http://localhost:5500/check-email', request).then(res => {
            if(res.data.results){
                setErrorMatch(true);
            
            }else{
                setPage(2);
                setEmail(formData.emailID);
            }
        })
    }            
     
    const registerSubmit = formData => {
        if(formData.password === formData.passwordConfirm){
            delete formData.passwordConfirm;

            API.post('http://localhost:5500/register-company', formData).then(res => {
                console.log(res.data.results);
                document.cookie = 'token=' + res.data.results.token;
                document.cookie = 'user=' + JSON.stringify(res.data.results.user);
            });

            reset();
            setPage(1);
            setIsLogged(true);

            history.push('/authentication/congrats'); 

        }else{
            // MOSTRAR POPUP
            console.log('No coinciden las contraseñas');
        }
    }

    useEffect(() => setPage(1), []);

   
    return (

        <CSSTransition
            appear
            in
            classNames='header-transition'
            timeout={800}
        >

        <div className={page === 1 ? "c-register" : "c-register c-register--pagetwo"}>

            {page === 1 && 
            <>
                <div className="c-register__social"> 
                    <h3 className="c-register__social__title">Crear cuenta con:</h3>
                    <div className="b-social-gallery">
                        <OAuth social='facebook' socket={socket}/>
                        <OAuth social='google' socket={socket}/>
                        <OAuth social='linkedin' socket={socket}/>
                    </div>
                </div>

            <hr className="c-register__separator"/>
            </>
            }
            
            <form className="c-register__form" onSubmit={page === 1 ? handleSubmit(toRegisterLocal): handleSubmit(registerSubmit)}>
                {page === 1 &&
                    <>
                    <label htmlFor="emailID" className="b-label b-label--nomargin">¿Tienes otro correo electrónico?</label>
                    <input className="b-input" type="text" id="emailID" name="emailID" placeholder="Correo electrónico" 
                        ref={register({ 
                            required: true, 
                            pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ 
                        })}
                    />
                    {errors.emailID && errors.emailID.type === "required" && <span className="c-warning">Falta poner un mail</span>}
                    {errors.emailID && errors.emailID.type === "pattern" && <span className="c-warning">Revisa el email. ejemplo@ejemplo2.ejemplo</span>}
                    {errorMatch ===true && page === 1 && <span className="c-warning">Este email ya esta registrado</span>}
                    </>
                }

                {page === 2 &&
                <>
                    <label htmlFor="name" className="b-label">Nombre de la empresa</label>
                    <input className="b-input b-input--white" type="text" name="name" id="name" placeholder="Nombre de la empresa" 
                        ref={register({ 
                            required: true, 
                        })}
                    />
                    {errors.name && errors.name.type === "required" && <span className="c-warning">Falta poner el nombre de la empresa</span>}

                    <label htmlFor="NIF" className="b-label">NIF</label>
                    <input className="b-input b-input--white" type="text" name="NIF" id="NIF" placeholder="G-0000000" 
                        ref={register({ 
                            // required: true, 
                        })}
                    />
                    {errors.NIF && errors.NIF.type === "required" && <span className="c-warning">Falta poner el número NIF</span>}

                    <label htmlFor="emailID" className="b-label">Email ID</label>
                    <input value={email} readOnly={true} className="b-input b-input--white" type="email" name="emailID" id="emailID" placeholder="email ID" 
                        ref={register({ 
                            required: true,
                            pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ 
                        })}
                    />
                    {errors.emailID && errors.emailID.type === "required" && <span className="c-warning">Falta poner un mail</span>}
                    {errors.emailID && errors.emailID.type === "pattern" && <span className="c-warning">Revisa el email. ejemplo@ejemplo2.ejemplo</span>}

                    <label htmlFor="password" className="b-label">Contraseña</label>
                    <PasswordEye forId="password">
                        <input className="b-input b-input--white" type="password" name="password" id="password" placeholder="Contraseña" 
                            ref={register({ 
                                required: true,
                                // pattern="(?=.\d)(?=.[a-z])(?=.[()[]{}?!$%&/=+~,.;:<>_-])(?=.*[A-Z]).{9,}" 
                            })}
                        />
                    </PasswordEye>
                    {errors.password && errors.password.type === "required" && <span className="c-warning">Falta poner una contraseña</span>}
                    {errors.password && errors.password.type === "pattern" && <span className="c-warning">1 min , 1max, 1caracter espcial, length 9</span>}
                    

                    <label htmlFor="passwordConfirm" className="b-label">Confirmar contraseña</label>
                    <PasswordEye forId="passwordConfirm">
                        <input className="b-input b-input--white" type="password" name="passwordConfirm" id="passwordConfirm" placeholder="Confirmar contraseña" 
                            ref={register({ 
                                required: true, 
                                // pattern="(?=.\d)(?=.[a-z])(?=.[()[]{}?!$%&/=+~,.;:<>_-])(?=.*[A-Z]).{9,}"
                            })}
                        />
                    </PasswordEye>
                    {errors.passwordConfirm && errors.passwordConfirm.type === "required" && <span className="c-warning">Falta poner una contraseña</span>}
                    {errors.passwordConfirm && errors.passwordConfirm.type === "pattern" && <span className="c-warning">1 min , 1max, 1caracter espcial, length 9</span>}
                    

                    <label className='c-register__checkbox' htmlFor="termsAccept">
                        <input type="checkbox" name="termsAccept" id="termsAccept" required />
                        {errors.termsAccept && errors.termsAccept.type === "required" && <span className="c-warning">Falta aceptar las condiciones</span>}
                        <p>
                            Al crear una cuenta, acepta automáticamente todos los 
                            <Link to="/termsAndConditions"> términos y condiciones </Link> 
                            relacionados con 
                            <a> meeTTalent</a>.
                        </p>
                    </label>
                </>        
                }

                <button type="submit" className="b-button b-button--fixed b-button--inverted">Continuar</button>

            </form>

        </div>

        </CSSTransition>

    )
}