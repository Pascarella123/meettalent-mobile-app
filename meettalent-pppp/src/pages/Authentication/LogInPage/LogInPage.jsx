import React, { useContext } from 'react';
import { useForm } from "react-hook-form";
import {Link, useHistory} from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import { IsLoggedContext } from '../../../shared/contexts/IsLoggedContext';
import { API } from '../../../shared/services/api';

import logo from '../../../assets/images/meettalent\ 2.png';

import './LogInPage.scss';

export function LogInPage(){

    //REDIRECCIONAR SI ESTÁS YA LOGEADO Y CUANDO TE LOGEES !!!
    //RECOGER Y MOSTRAR LOS ERRORES !!!
    
    //CERRAR RUTA /authentication si ya estás logeado !!!!

    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    
    const {setIsLogged} = useContext(IsLoggedContext);

    const history = useHistory();

    const onSubmit = formData => {
        // console.log(formData);
        API.post('http://localhost:5500/login-company', formData).then(res => {
            document.cookie = 'token=' + res.data.results.token; 
            document.cookie = 'user=' + JSON.stringify(res.data.results.user); 

            setIsLogged(true);
            history.push('/authentication/congrats');
        });

        reset();

    }

    return(

        <CSSTransition
            appear
            in
            classNames='header-transition'
            timeout={800}
        >

        <div className="c-login"> 

            <img className="c-login__logo" src={logo} alt="Meet Talent logo"/>

            <form className="c-login__form" onSubmit={handleSubmit(onSubmit)}>
                    <label htmlFor="emailID" className="b-label">Email ID</label>
                    <input className="b-input" type="text" name="emailID" id="emailID" placeholder="Email ID" 
                        ref={register({ 
                            required: true, 
                            pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ 
                        })}
                    />
                    {errors.emailID && errors.emailID.type === "required" && <span className="c-warning2">Falta poner un mail</span>}
                    {errors.emailID && errors.emailID.type === "pattern" && <span className="c-warning2">Revisa el email. ejemplo@ejemplo2.ejemplo</span>}
               
            
                <label htmlFor="password" className="b-label">Contraseña</label>
                <input className="b-input" type="password" name="password" id="password" placeholder="Contraseña" 
                    ref={register({
                    required: true,
                    //    pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/ //LO HE QUITADO PARA TESTEAR !!!, QUÉ PIDE ESTA??? PROBAR OTRO REGEX??
                })}
                />
                {errors.password && errors.password.type === "required" && <span className="c-warning2">Falta poner la contraseña</span>}
                {errors.password && errors.password.type === "pattern" && <span className="c-warning2">Revisa la contraseña. Necesita minúscula, mayúscula , número y de 8 caracters de largo como minimo</span>}
        
                <button type="submit" className="b-button">Comenzar</button>
            </form>
                
            <p className="c-login__restore-link">¿No puedes iniciar sesión?     
                <Link to="/authentication/restore-password"> Restablecer la contraseña </Link>
            </p>
        
            <div className="c-login__decorator b-separator"> o </div>
            
            <Link to="/authentication/register"><button className="b-button b-button--inverted">Crear nueva cuenta</button></Link>
        
        </div>
        </CSSTransition>

    )
}