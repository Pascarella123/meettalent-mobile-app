import React, { useEffect, useContext, useState } from 'react';
import { useForm } from "react-hook-form";
import {useHistory} from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import { API } from '../../../shared/services/api';
import { PageContext } from '../../../shared/contexts/PageContext';

import './RestorePassword.scss';

let email = "";

export function RestorePasswordPage(){

    const { register, handleSubmit, reset, formState: { errors }  } = useForm();
    
    const {page, setPage} = useContext(PageContext);

    const history = useHistory();

    const [errorMatch, setErrorMatch]= useState(false);

    const onSubmitEmail = formData => {

        if(formData.emailID === formData.emailIDConfirm){
            email=formData.emailID;
            const request = {emailID: formData.emailID};
            API.post('http://localhost:5500/check-email', request).then(res => {
                if(res.data.results){
                    setPage(2);

                    reset();
                }else{
                    //POPUPS ERRORES
                    console.log('Ese email no está registrado');
                    setErrorMatch(true);
                }
            });
        }else{
            //POPUPS ERRORES
            console.log('No coinciden los emails');
            setErrorMatch(true);
        }
    }

    const onSubmitPassword = formData => {

        if(formData.password === formData.passwordConfirm){
            const request = {password: formData.password};
            API.patch('http://localhost:5500/change-password?emailID='+ email, request).then(res => { 
                reset();
                setPage(1);

                history.push('/authentication');
            });
        }else{
            //POPUPS ERRORES
            console.log('No coinciden los passwords');
            setErrorMatch(true);
        }
    }

    useEffect(() => setPage(1), [])
    useEffect(() => console.log(errors), [errors])

    return(

        <CSSTransition
            appear
            in
            classNames='header-transition'
            timeout={800}
        >

        <div className="c-restore-password">

            <form className="c-restore-password__form" onSubmit={page === 2 ? handleSubmit(onSubmitPassword) : handleSubmit(onSubmitEmail)}>
                <label htmlFor={page === 2 ? "password" : "emailID"} className="b-label">{page === 2 ? "Nueva contraseña" : "Email ID"}</label>
                <input className="b-input" type={page === 2 ? "password":"text"} id={page === 2 ? "password" : "emailID"} name={page === 2 ? "password" : "emailID"} placeholder={page === 2 ? "Nueva contraseña" : "Email ID"} 
                    ref={register({ 
                        required: true, 
                        // pattern="(?=.\d)(?=.[a-z])(?=.[()[]{}?!$%&/=+~,.;:<>_-])(?=.*[A-Z]).{9,}"
                    })}
                />

                {errors.emailID && errors.emailID.type === "required" && page === 1 &&<span className="c-warning2">Falta poner un mail</span>}
                {errors.emailID && errors.emailID.type === "pattern" &&  page === 1 &&<span className="c-warning2">Revisa el email. ejemplo@ejemplo2.ejemplo</span>}
                {errors.password && errors.password.type === "required" &&  page === 2 &&<span className="c-warning2">Falta poner una contraseña</span>}
                {errors.password && errors.password.type === "pattern" &&  page === 2 &&<span className="c-warning2">Revisa la contraseña</span>}

                <label htmlFor={page === 2 ? "passwordConfirm" : "emailIDconfirm"} className="b-label">Confirmar</label>
                <input className="b-input" type={page === 2 ? "password" : "text"} id={page === 2 ? "passwordConfirm" : "emailIDConfirm"} name={page === 2 ? "passwordConfirm" : "emailIDConfirm"} placeholder={page === 2 ? "Confirmar password" : "Confirmar email"} 
                    ref={register({ 
                        required: true, 
                        // pattern="(?=.\d)(?=.[a-z])(?=.[()[]{}?!$%&/=+~,.;:<>_-])(?=.*[A-Z]).{9,}"
                    })}
                />

                {errors.emailIDConfirm && errors.emailIDConfirm.type === "required" && page === 1 &&<span className="c-warning2">Falta poner un mail</span>}
                {errors.emailIDConfirm && errors.emailIDConfirm.type === "pattern" &&  page === 1 &&<span className="c-warning2">Revisa el email. ejemplo@ejemplo2.ejemplo</span>}
                {errors.passwordConfirm && errors.passwordConfirm.type === "required" &&  page === 2 &&<span className="c-warning2">Falta poner una contraseña</span>}
                {errors.passwordConfirm && errors.passwordConfirm.type === "pattern" &&  page === 2 &&<span className="c-warning2">Revisa la contraseña</span>}
                {errorMatch && page === 1 &&<span className="c-warning2">Los mails no coinciden</span>}
                {errorMatch && page === 2 &&<span className="c-warning2">Las contraseñas no coinciden</span>}
        
                <button type="submit" className="b-button">{page === 2 ? "Guardar" : "Enviar enlace"}</button>
            </form>

        </div>

        </CSSTransition>

    )
}