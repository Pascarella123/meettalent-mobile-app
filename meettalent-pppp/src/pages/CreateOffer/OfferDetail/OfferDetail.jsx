import React from 'react';

import './OfferDetail.scss';
import check from "../../../assets/images/text+check.png";



const offer = {title:"Developer", candidates:"10", company:"Apple", vacant:"1", language:"esp", sector:"locura", formation:"elevada", description:"esto es una locura mal pagada", city:"madrid", country:"Spain", availability:"inmediatly", journey:"si tu invitas", contract:"full time", date:"20/2/2021", requirements:"muchos", keywords:"divertidos"};

const inscriptions = {percentage:"65%",number:"52", process:"45", finalist:"3"};

export function OfferDetail(){

   
    return (
        <div className="c-offercard">
            <header className="b-header">
                <span className="b-header__backIcon">
                    <span className="icon-chevron-left"></span>
                </span> 
                <h2 className="b-header__title">Ofertas cerradas</h2>
                <span className="b-header__closeIcon icon-x"></span>
            </header>
            <h3 className="offertitle">{offer.title}</h3>
            <div className="c-offercard__mainInfo">
                <div className="c-offercard__mainInfo__box">
                    <p>{offer.date}</p>
                </div>
                <div className="c-offercard__mainInfo__box">
                    <span className="icon-home"></span>
                    <p>{offer.city}</p>
                </div>
                <div className="c-offercard__mainInfo__box">
                    <p>{offer.vacant}</p>
                    <span className="icon-user"></span>
                </div>
                <div className="c-offercard__mainInfo__box">
                    <span className="icon-eye-off"></span>
                    <p>{offer.availability}</p>
                </div>
            </div>
            <div className="c-offercard__lowOffer">
                <div className="c-offercard__lowOffer c-offercard__lowOffer--direction">
                    <div>
                        <p className="offerbold">{inscriptions.number}</p>
                        <p>Inscritos</p>
                    </div>
                    <div>
                        <p className="offerbold">{inscriptions.process}</p>
                        <p>En proceso</p>
                    </div>
                    <div>
                        <p className="offerbold">{inscriptions.finalist}</p>
                        <p>Finalistas</p>
                    </div>
                </div>
                <div className="c-offercard__lowOffer c-offercard__lowOffer--alternative">
                    <img src="" alt="graficos"/>
                    <h4>Efectividad de portales de empleo</h4>
                </div>
                <div className="c-offercard__lowOffer c-offercard__lowOffer--alternative">
                    <img src="" alt="graficos"/>
                    <h4>Resultados Hackathon</h4>
                </div>
                <div className="c-offercard__lowOffer c-offercard__lowOffer--alternative">
                    <img src="" alt="graficos"/>
                    <h4>Resultados entrevistas individuales</h4>
                </div>
                <div className="c-offercard__lowOffer c-offercard__lowOffer--alternative">
                    <div className="c-offercard__lowOffer c-offercard__lowOffer--centered">
                        <span className="icon-stack"></span>
                        <p>Informe del proceso</p>
                    </div>                
                        <hr></hr>
                    <div className="c-offercard__lowOffer c-offercard__lowOffer--centered">
                        <img src={check} alt="imagen de una carpeta + check"/>
                        <p>Informe de optimización</p>
                    </div>            
                </div>     
                <div className="c-offercard__lowOffer c-offercard__lowOffer--centered offertitle offerbold">
                    <span className="icon-trash-2"></span>
                </div>
            </div>
                    
    </div>            
    )
}
