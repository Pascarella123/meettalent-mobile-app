import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';

import { OffersContext } from '../../../shared/contexts/OffersContext';

import "./PublishedOffers.scss";

export function PublishedOffers(){

    const {offers} = useContext(OffersContext);

    const history = useHistory();

    const [newOfferTitle, setNewOfferTitle] = useState('');
    const [displaySpan, setDisplaySpan] = useState(false);

    const goToOffer = (offerId) => {
        // console.log(offerId);
        if (offerId === undefined) {
            localStorage.setItem('offerTitle', newOfferTitle);
            history.push('/create/offer');
        } else {
            history.push('/create/offer/' + offerId);
        }
    }

    useEffect(() =>{
        if(newOfferTitle !== '' && newOfferTitle !== undefined){
            setDisplaySpan(false);
        }
    },[newOfferTitle]);

    // useEffect(() => console.log(offers), [offers]);

    return (
        <>
            <div className={offers.length === 0 ? "c-startOffer c-startOffer--center" : "c-startOffer"}>
                {offers.length > 0 &&
                    <>
                    <label>Duplicar oferta</label>
                    {offers.map((offer, i) => <button key={i} className="b-button b-button--blue" onClick={() => goToOffer(offer._id)}>{offer.title}</button>)}

                    <hr className="u-line-separator"/>
                    </>
                }

                {offers.length === 0 && 
                    <p>
                        Esta es tu primera oferta. Escribe aquí el título y sigue los pasos del formulario.
                    </p>
                }

                <label>Título de la nueva oferta</label>
                <input className="b-input" placeholder="Escribe el título..." onInput={(e) => setNewOfferTitle(e.target.value)}></input>
                <a target="_blank" rel="noreferrer" href='https://info.netcommerce.mx/5-consejos-para-crear-un-titulo-llamativo-como-este/'>¿Cómo crear un título efectivo?</a>

                {displaySpan &&
                    <span>
                        Establece un título a tu oferta para continuar
                    </span>
                }

                <button className="b-button b-button--fixed" onClick={newOfferTitle === '' || newOfferTitle === undefined ? () => { setDisplaySpan(true) } : () => goToOffer()}>Comenzar</button>
            </div>
            
         </>
    )
}
