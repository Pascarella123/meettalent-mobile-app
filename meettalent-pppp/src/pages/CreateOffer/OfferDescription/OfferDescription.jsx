import React, { useContext, useEffect, useState } from 'react';
import { useForm } from "react-hook-form";
import { API } from '../../../shared/services/api';

import './OfferDescription.scss';

import { PageContext } from '../../../shared/contexts/PageContext';
import { TextArea } from '../../../shared/components/littleComponents/TextArea/TextArea';
import { KeywordsGallery } from '../../../shared/components/littleComponents/KeywordsGallery/KeywordsGallery';
import { OfferQuestionsGallery } from '../../../shared/components/offerQuestions/OfferQuestionsGallery';
import { AddOfferQuestions } from '../../../shared/components/offerQuestions/AddOfferQuestions';
import { getCookieUtil } from '../../../shared/utils/getCookieUtil';
import { useHistory, useParams } from 'react-router';

import Calendar from 'react-calendar';
import '../../../shared/components/Calendar/Calendar.scss'



export function OfferDescription(){

//-----------DATOS RANDOM HASTA QUE SE PUEDA LLAMAR AL BACK-------------

    const data = {
        vacancies: [1, 2, 3, 4, 5],
        languages: ["Español", "Inglés", "Francés"],
        sectors: ["Desarrollo Front", "Desarrollo Back", "Diseño", "Estudio de Ofertas"],
        formations: ["FullStackDeveloper", "Front Developer", "Back Developer", "Web Designer"],
        cities: ["Madrid", "Barcelona", "Valencia", "Cuenca"],
        countries: ["España", "Inglaterra", "Francia"],
        availabilities: ["Inmediata", "Principio del mes entrante", "Principio del año entrante", "Sin determinar"],
        salaries: ["Menos de 1.000€/mes", "1.000-2.000€/mes", "2.000-3.000€/mes", "Más de 3.000€/mes"],
        journies: ["Jornada completa", "Media jornada (mañana)", "Media jornada (tarde)", "Nocturno"],
        contracts: ["Indefinido", "Fijo", "Obra y Servicio", "Temporal"]
    }
//-----------HASTA AQUÍ HABRÁ QUE ELIMINAR PARA PEDIR LOS DATOS DE BACK-------------



    const history = useHistory();

    const { register, handleSubmit, reset, formState: { errors }   } = useForm();
    const {page, setPage, setShowClose, setTitle} = useContext(PageContext);
    const [formInfo, setFormInfo] = useState();
    const totalPages = 4;

    const [keywords, setKeywords] = useState([]);
    const [questions, setQuestions] = useState([]);
    const [offerTitle, setOfferTitle] = useState();
    const [dateValue, setDateValue] = useState(new Date());
    const offerExpired = dateValue.toISOString();

    const {offerId} = useParams();

    const getOffer = () => {
        if (offerId !== undefined) {
            API.get('http://localhost:5500/offerOne?id=' + offerId).then(res => {
                setOfferTitle(res.data.results.title);

                const originalData = res.data.results;
                const {createdAt, updatedAt, __v, _id, candidates, keywords, questions, ...preparedFormInfo} = originalData;
                const preparedKeywords = originalData.keywords;
                const preparedQuestions = [];
                for (const question of originalData.questions) {
                    const {_id, ...preparedQuestion} = question;
                    const preparedAnswers = [];
                    for (const answer of preparedQuestion.answers) {
                        const {_id, ...preparedAnswer} = answer;
                        preparedAnswers.push(preparedAnswer);
                    }
                    preparedQuestions.push(preparedQuestion);
                }

                for (const item in preparedFormInfo) {
                    const $$node = document.getElementById(item);
                    if ($$node !== null) {
                        $$node.value = preparedFormInfo[item];
                    }
                }

                setFormInfo(preparedFormInfo);
                setKeywords(preparedKeywords);
                setQuestions(preparedQuestions);
            })
        } else {
            setOfferTitle(localStorage.getItem('offerTitle'));
        }
    }

    const toNextPage = (formData) => {
        setFormInfo({...formInfo, ...formData});
        setPage(page + 1);
        
    }

    const refresh = () => {
        for (const input in formInfo){
            if (document.getElementById(input) !== null){
                document.getElementById(input).value = formInfo[input];
            }
        }

        if (page === 4) {
            setShowClose(true);
            setTitle('Confirmar Oferta');
        } else {
            setShowClose(false);
            setTitle('Descripción de la Oferta');
        }
    }

    const addKeyword = (word) => {
        if (word !== '' && !keywords.includes(word)) {
            const currentKeywords = [...keywords, word];
            setKeywords(currentKeywords);
            document.getElementById("keywords").value = '';
        }else if (keywords.includes(word)){
            document.getElementById("keywords").value = '';
        }
    }
    const removeKeyword = (event) => {
        const currentKeywords = [...keywords];
        currentKeywords.splice(currentKeywords.indexOf(event.target.textContent), 1);
        setKeywords(currentKeywords);
    }

    const addQuestion = (newQuestion) => {
        const currentQuestions = [...questions];
        currentQuestions.push(newQuestion);
        setQuestions(currentQuestions);
    }
    const removeQuestion = (event) => {
        const indexToRemove = event.target.dataset.index;
        const currentQuestions = [...questions];
        currentQuestions.splice(indexToRemove, 1);
        setQuestions(currentQuestions);
    }


    const offerDescriptionSubmit = () => {
        const stringUser = getCookieUtil('user');
        const user = JSON.parse(stringUser ? stringUser : {});

        const toSubmitInfo = ({...formInfo, company: user.id, expired: false, expireAt: offerExpired, title: offerTitle, keywords: keywords, questions: questions});
        // console.log(toSubmitInfo);
        API.post('http://localhost:5500/offer', toSubmitInfo).then(res => {
            // console.log(res);
        })
        history.push('/offer/congrats');
    }

    const clearLocalStorage = () => {
        return () => localStorage.removeItem('offerTitle');
    }

    useEffect(getOffer, []);
    useEffect(refresh, []);
    useEffect(() => setPage(1), []);
    useEffect(refresh, [page]);

    useEffect(clearLocalStorage, []);
   
    return (
        <>
            <form className="c-offerDescription" onSubmit={page < totalPages ? handleSubmit(toNextPage) : handleSubmit(offerDescriptionSubmit)}>
                {page === 1 &&
                <>
                    <select className="b-select b-select--white" name="vacancies" type="select" id="vacancies" ref={
                        register({
                            required: true
                        })
                    }>
                   
                        <option hidden value="" key={0}>Nº de vacantes</option>
                        {data.vacancies.map((vacancie, i) => 
                            <option value={vacancie} key={i + 1}>{vacancie}</option>
                        )}
                        <option value={'+' + data.vacancies[data.vacancies.length - 1]}>más de {data.vacancies[data.vacancies.length - 1]}...</option>
                    </select>
                    {errors.vacancies && errors.vacancies.type === "required" && page === 1 &&<span className="c-warning">Campo requerido</span>}

                    <select className="b-select b-select--white" name="language" type="select" id="language" ref={
                        register({
                            required: true
                        })
                    }>

                        <option hidden value="" key={0}>Idioma</option>
                        {data.languages.map((language, i) =>
                            <option value={language} key={i + 1}>{language}</option>
                        )}
                    </select>
                    {errors.language && errors.language.type === "required" && page === 1 &&<span className="c-warning">Campo requerido</span>}
                    <select className="b-select b-select--white" name="sector" type="select" id="sector" ref={
                        register({
                            required: true
                        })
                    }>
                   
                        <option value="" hidden key={0}>Sector</option>
                        {data.sectors.map((sector, i) =>
                            <option value={sector} key={i + 1}>{sector}</option>
                        )}
                    </select>
                    {errors.sector && errors.sector.type === "required" && page === 1 &&<span className="c-warning">Campo requerido</span>}

                    <select className="b-select b-select--white" name="formation" type="select" id="formation" ref={
                        register({
                            required: true
                        })
                    }>                   
                    
                        <option value="" hidden key={0}>Formación</option>
                        {data.formations.map((formation, i) =>
                            <option value={formation} key={i + 1}>{formation}</option>
                        )}
                    </select>
                    {errors.formation && errors.formation.type === "required" && page === 1 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="offerDescription" className="b-label b-label__sectionSubtitle">Descripción de la oferta <span className="icon-edit-2 b-label__icon"></span></label>
                    <TextArea fnId="offerDescription" fnRef={register({required: true})} fnName="offerDescription" fnPlaceholder="Descripción..."/>
                    {errors.offerDescription && errors.offerDescription.type === "required" && page === 1 &&<span className="c-warning">Campo requerido</span>}
                </>
                }

                {page === 2 &&
                <>
                    <select className="b-select b-select--white" name="city" type="text" id="city" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Ciudad</option>
                        {data.cities.map((city, i) =>
                            <option value={city} key={i + 1}>{city}</option>
                        )}
                    </select>
                    {errors.city && errors.city.type === "required" && page === 2 &&<span className="c-warning">Campo requerido</span>}

                    <select className="b-select b-select--white" name="country" type="select" id="country" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={119}>País</option>
                        {data.countries.map((country, i) =>
                            <option value={country} key={i}>{country}</option>
                        )}
                    </select>
                    {errors.country && errors.country.type === "required" && page === 2 &&<span className="c-warning">Campo requerido</span>}

                    <select className="b-select b-select--white" name="availability" type="select" id="availability" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Disponibilidad</option>
                        {data.availabilities.map((availability, i) =>
                            <option value={availability} key={i + 1}>{availability}</option>
                        )}
                    </select>
                    {errors.availability && errors.availability.type === "required" && page === 2 &&<span className="c-warning">Campo requerido</span>}
                    
                    <hr className="u-line-separator"></hr>

                    <label htmlFor="salary" className="b-label b-label__sectionTitle">Condiciones</label>
                    <select className="b-select b-select--white" name="salary" type="select" id="salary" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Salario</option>
                        {data.salaries.map((salary, i) =>
                            <option value={salary} key={i + 1}>{salary}</option>
                        )}
                    </select>
                    {errors.salary && errors.salary.type === "required" && page === 2 &&<span className="c-warning">Campo requerido</span>}

                    <select className="b-select b-select--white" name="journey" type="select" id="journey" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Tipo de jornada</option>
                        {data.journies.map((journey, i) =>
                            <option value={journey} key={i + 1}>{journey}</option>
                        )}
                    </select>
                    {errors.journey && errors.journey.type === "required" && page === 2 &&<span className="c-warning">Campo requerido</span>}

                    <select className="b-select b-select--white" name="contract" type="select" id="contract" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Tipo de contrato</option>
                        {data.contracts.map((contract, i) =>
                            <option value={contract} key={i + 1}>{contract}</option>
                        )}
                    </select>
                    {errors.contract && errors.contract.type === "required" && page === 2 &&<span className="c-warning">Campo requerido</span>}
                </>
                }

                {page === 3 &&
                <>
                    <label htmlFor="candidateDescription" className="b-label b-label__sectionTitle">Requisitos de candidato</label>
                    <label htmlFor="requirements" className="b-label b-label__sectionSubtitle">Descripción de requisitos <span className="icon-edit-2 b-label__icon"></span></label>
                    <TextArea fnId="requirements" fnRef={register({required: true})} fnName="requirements" fnPlaceholder="Descripción..."/>
                    {errors.requirements && errors.requirements.type === "required" && page === 3 &&<span className="c-warning">Campo requerido</span>}
                    
                    {questions && <OfferQuestionsGallery questions={questions} fnRemoveQuestion={removeQuestion}/>}
                    <AddOfferQuestions fnAddQuestion={addQuestion} fnQuestions={questions}/>

                    <hr className="u-line-separator"></hr>
                    
                    <label htmlFor="keywords" className="b-label b-label__sectionTitle">Codificaciones internas</label>
                    <div className="keywordsInputContainer">
                        <input className="b-input b-input--white" name="keywords" type="text" id="keywords" placeholder="Añadir palabras clave"></input>
                        <span className="icon-plus-square keywordsInputContainer__addKeyword" onClick={() => addKeyword(document.getElementById("keywords").value)}></span>
                    </div>
                    <KeywordsGallery list={keywords} fnRemove={removeKeyword}/>
                </>
                }

                {page === 4 &&
                <>
                    <label htmlFor="vacancies" className="b-label b-label__sectionTitle">Número de vacantes</label>
                    <select className="b-select b-select--white" name="vacancies" type="select" id="vacancies" ref={
                            register({
                                required: true
                            })
                        }>
                        <option value="" hidden key={0}>Nº de vacantes</option>
                        {data.vacancies.map((vacancie, i) => 
                            <option value={vacancie} key={i + 1}>{vacancie}</option>
                        )}
                        <option>más de {data.vacancies[data.vacancies.length - 1]}...</option>
                    </select>
                    {errors.vacant && errors.vacant.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="language" className="b-label b-label__sectionTitle">Idioma</label>
                    <select className="b-select b-select--white" name="language" type="select" id="language" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Idioma</option>
                        {data.languages.map((language, i) =>
                            <option value={language} key={i + 1}>{language}</option>
                        )}
                    </select>
                    {errors.language && errors.language.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="sector" className="b-label b-label__sectionTitle">Sector</label>
                    <select className="b-select b-select--white" name="sector" type="select" id="sector" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Sector</option>
                        {data.sectors.map((sector, i) =>
                            <option value={sector} key={i + 1}>{sector}</option>
                        )}
                    </select>
                    {errors.sector && errors.sector.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="formation" className="b-label b-label__sectionTitle">Formación</label>
                    <select className="b-select b-select--white" name="formation" type="select" id="formation" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Formación</option>
                        {data.formations.map((formation, i) =>
                            <option value={formation} key={i + 1}>{formation}</option>
                        )}
                    </select>
                    {errors.formation && errors.formation.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="offerDescription" className="b-label b-label__sectionSubtitle">Descripción de la oferta <span className="icon-edit-2 b-label__icon"></span></label>
                    <TextArea fnId="offerDescription" fnRef={register({required: true})} fnName="offerDescription" fnPlaceholder="Descripción..."/>
                    {errors.offerDescription && errors.offerDescription.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}
                    
                    <label htmlFor="country" className="b-label b-label__sectionTitle">País</label>
                    <select className="b-select b-select--white" name="country" type="select" id="country" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={119}>País</option>
                        {data.countries.map((country, i) =>
                            <option value={country} key={i}>{country}</option>
                        )}
                    </select>
                    {errors.country && errors.country.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="city" className="b-label b-label__sectionTitle">Ciudad</label>
                    <select className="b-select b-select--white" name="city" type="text" id="city" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Ciudad</option>
                        {data.cities.map((city, i) =>
                            <option value={city} key={i + 1}>{city}</option>
                        )}
                    </select>
                    {errors.city && errors.city.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="disponibility" className="b-label b-label__sectionTitle">Disponibilidad</label>
                    <select className="b-select b-select--white" name="availability" type="select" id="availability" ref={
                        register({
                            required: true
                        })
                    }>
                        <option value="" hidden key={0}>Disponibilidad</option>
                        {data.availabilities.map((availability, i) =>
                            <option value={availability} key={i + 1}>{availability}</option>
                        )}
                    </select>
                    {errors.availability && errors.availability.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <hr className="u-line-separator"></hr>

                    <label htmlFor="salary" className="b-label b-label__sectionTitle">Condiciones</label>
                    <select className="b-select b-select--white" name="salary" type="select" id="salary" ref={
                        register({
                            required: true
                        })
                    }>
                        <option hidden value="" key={0}>Salario</option>
                        {data.salaries.map((salary, i) =>
                            <option value={salary} key={i + 1}>{salary}</option>
                        )}
                    </select>
                    {errors.salaray && errors.salary.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="journey" className="b-label b-label__sectionTitle">Tipo de jornada</label>
                    <select className="b-select b-select--white" name="journey" type="select" id="journey" ref={
                        register({
                            required: true
                        })
                    }>
                        <option hidden value="" key={0}>Tipo de jornada</option>
                        {data.journies.map((journey, i) =>
                            <option value={journey} key={i + 1}>{journey}</option>
                        )}
                    </select>
                    {errors.journey && errors.journey.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="contract" className="b-label b-label__sectionTitle">Tipo de Contrato</label>
                    <select className="b-select b-select--white" name="contract" type="select" id="contract" ref={
                        register({
                            required: true
                        })
                    }>
                        <option hidden value="" key={0}>Tipo de contrato</option>
                        {data.contracts.map((contract, i) =>
                            <option value={contract} key={i + 1}>{contract}</option>
                        )}
                    </select>
                    {errors.contract && errors.contract.type === "required" && page === 4 &&<span className="c-warning">Campo requerido</span>}

                    <label htmlFor="requirements" className="b-label b-label__sectionSubtitle">Descripción de requisitos <span className="icon-edit-2 b-label__icon"></span></label>
                    <TextArea fnId="requirements" fnRef={register({required: true})} fnName="requirements" fnPlaceholder="Descripción..."/>

                    {questions && <OfferQuestionsGallery questions={questions} fnRemoveQuestion={removeQuestion}/>}
                    <AddOfferQuestions fnAddQuestion={addQuestion} fnQuestions={questions}/>
                
                    <label htmlFor="keywords" className="b-label b-label__sectionTitle">Añadir palabras clave</label>
                    <div className="keywordsInputContainer">
                        <input className="b-input b-input--white" name="keywords" type="text" id="keywords" placeholder="Añadir palabras clave"></input>
                        <span className="icon-plus-square keywordsInputContainer__addKeyword" onClick={() => addKeyword(document.getElementById("keywords").value)}></span>
                    </div>
                    <KeywordsGallery list={keywords} fnRemove={removeKeyword}/>

                    <hr className="u-line-separator"></hr>
                    
                    <label htmlFor="calendar" className="b-label b-label__sectionTitle">Definir fecha límite para la oferta</label>
                    <Calendar onChange={setDateValue} value={dateValue} minDate={new Date()}/>
                </>        
                }

                <button type="submit" className="b-button b-button--inverted">{page < totalPages ? "Continuar" : "Confirmar"}</button>

            </form>
        </>
    )
}