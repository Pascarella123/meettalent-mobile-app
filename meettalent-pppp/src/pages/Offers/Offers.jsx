import { useContext, useState } from "react";
import {CSSTransition} from 'react-transition-group';

import { OffersContext } from '../../shared/contexts/OffersContext';
// import { SearchFilterContext } from '../../shared/contexts/SearchFilterContext';

import { OfferCard } from "../../shared/components/cards/OfferCard/OfferCard";
import { Navbar } from "../../shared/components/Navbar/Navbar";

import './Offers.scss';
import { SearchBar } from "../../shared/components/SearchBar/SearchBar";


export function Offers() {

  const {offers} = useContext(OffersContext);
  
  const [showOpened, setShowOpened] = useState(true);
  const [state, setState] = useState(true);

  //??
  const [toFilter, setToFilter] = useState({});
  const [filtered, setFiltered] = useState({});
  //---\\

  return (
    <div className='c-offers'>

      <SearchBar inOffers={true} toFilter={toFilter} setFiltered={setFiltered}/>

      <CSSTransition
          appear
          in
          classNames='header-transition'
          timeout={800}
      >

        <div className="c-offers__head">
          <span onClick={() => setShowOpened(true)} className={showOpened ? "c-offers__head__item c-offers__head__item--green" : "c-offers__head__item"}>Abiertas</span>
          <span onClick={() => setShowOpened(false)} className={showOpened ? "c-offers__head__item" : "c-offers__head__item c-offers__head__item--green"}>Cerradas</span>
        </div>

      </CSSTransition>

      {showOpened &&
        <CSSTransition
            appear
            in
            classNames='offer-transition'
            timeout={800}
        >

          <div className="c-offers__gallery">
            {showOpened ? offers.map((offer, i) => !offer.expired ? <OfferCard key={i} offer={offer}/> : '') : offers.map((offer, i) => offer.expired ? <OfferCard key={i} offer={offer}/>  : '')}
          </div>

        </CSSTransition>
      }

      {!showOpened &&
        <CSSTransition
            appear
            in
            classNames='offer-transition'
            timeout={800}
        >

          <div className="c-offers__gallery">
            {showOpened ? offers.map((offer, i) => !offer.expired ? <OfferCard key={i} offer={offer}/> : '') : offers.map((offer, i) => offer.expired ? <OfferCard key={i} offer={offer}/>  : '')}
          </div>

        </CSSTransition>
      }
      
      <CSSTransition
          appear
          in
          classNames='navbar-transition'
          timeout={800}
      >
          <Navbar/>
      </CSSTransition>
    </div>
  )
}