import React from 'react';
import {Link} from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import { FullPageFigure } from '../../shared/components/fullPageFigure/FullPageFigure';

import imageLogin from '../../assets/images/Ilustraciones/Screenshot_10.png';
import imageOffer from '../../assets/images/Ilustraciones/Screenshot_2.png';

export function PopUpPage({contHistory, textBtn, popUpText, inOffers}){

    //METER DISTINTAS IMÁGENES

    return(
        <CSSTransition
            appear
            in
            classNames='header-transition'
            timeout={800}
        >


        <div>
            <FullPageFigure popup={true} text={popUpText} title='¡ Enhorabuena !' image={inOffers ? imageOffer : imageLogin}/>

            <Link to={contHistory}>
                <button className="b-button">
                    {textBtn}
                </button>
            </Link>
            
        </div>

        
        </CSSTransition>
    )
}
