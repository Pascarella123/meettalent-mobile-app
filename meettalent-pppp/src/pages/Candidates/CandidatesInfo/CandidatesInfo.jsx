import React, { useState } from 'react';
import './CandidatesInfo.scss';



export function CandidatesInfo(){

    const [heartFull, setHeartFull]= useState(false);


    const changeHeart =() =>{
        heartFull? setHeartFull(false) : setHeartFull(true);
        
    }
   
    return (
        <div className="c-candidate">
            <header className="b-header padding-bottom-header">
                    <span className="b-header__backIcon icon-chevron-left"></span> 
                    <h2 className="b-header__title">Candidatos</h2>
                    <span onClick={changeHeart} className={heartFull? "b-header__heartIcon icon-heart":"b-header__heartIcon icon-heart-o"}>
                    </span>
            </header>            
            <div className="c-candidate__container">
                <img src="https://i.pinimg.com/originals/e5/52/b6/e552b652485e7e55347e7027c3f3b908.png" alt="Foto del candidato" className="c-candidate__container--img"/>
                {/* <div className="casa">
                    <span className="icon-edit-2 c-candidate__container--edit"></span> 
                </div> */}
                
                <div className="c-candidate__container--centered">
                    <h3>Fátima Rámos</h3>
                    <h4>Diseñadora web</h4>
                    <div>
                        <span className="icon-twitter"></span>
                        <span className="icon-instagram"></span>
                        <span className="icon-facebook"></span>    
                    </div>
                    <hr></hr>
                </div>                
                <div>                   
                    <div className="c-candidate__container--information">
                        <h5>Datos personales</h5>
                        <div className="c-candidate__container--information--part">
                            <span className="icon-calendar"></span>
                            <p>30 años</p>
                        </div>
                        <div className="c-candidate__container--information--part">
                            <span className="icon-map-pin"></span>
                            <p>Madrid, capital, 28004</p>
                        </div>
                        <div className="c-candidate__container--information--part">
                            <span className="icon-mail"></span>
                            <p>fatimaramos@gmail.com</p>
                        </div>
                        <div className="c-candidate__container--information--part">
                            <span className="icon-phone"></span>
                            <p>655-555-000</p>
                        </div>
                    </div>
                    <div className="c-candidate__container--information">
                        <h5>Palabras clave del perfil</h5>
                        <div className="c-candidate__container--information--part distrobution-words">
                            <div className="item-word">UX/UI</div>
                            <div className="item-word item-word--var">Cool</div>
                            <div className="item-word">Cool</div>
                            <div className="item-word item-word--var">Cool</div>
                            <div className="item-word">Cool</div>
                            
                        </div>
                    </div>
                    <div className="c-candidate__container--information">
                        <h5>Formación académica</h5>
                        <div className="c-candidate__container--information--part">
                            <h6>Estudios</h6>
                            <div>
                                <p className="c-candidate__container--information--part--bold">Estudio en si</p>
                                <p>Escuela donde se estudio</p>
                            </div>                            
                        </div>
                        <div  className="c-candidate__container--information--part">
                            <h6>Estudios</h6>
                            <div>
                                <p className="c-candidate__container--information--part--bold">Estudio en si</p>
                                <p>Escuela donde se estudio</p>
                            </div>                            
                        </div>
                    </div>
                </div>  
            </div>
        </div>            
    )
}
