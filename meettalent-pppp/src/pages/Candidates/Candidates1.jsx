import React, { useState, useContext } from 'react';
import './Candidates1.scss';

import {CSSTransition} from 'react-transition-group';

import {CandidatesContext} from '../../shared/contexts/CandidatesContext';

import {Navbar} from '../../shared/components/Navbar/Navbar';
import {SearchBar} from '../../shared/components/SearchBar/SearchBar';
import {CandidatesCard} from '../../shared/components/cards/CandidatesCard/CandidatesCard';

export function Candidates1(){

    const {candidates} = useContext(CandidatesContext);

    //??
    const [toFilter, setToFilter] = useState({});
    const [filtered, setFiltered] = useState({});
    //---\\

    return (
        <div className="c-candidates">
            <SearchBar inOffers={true} toFilter={toFilter} setFiltered={setFiltered}/>

            <CSSTransition
                appear
                in
                classNames='header-transition'
                timeout={800}
            >

                <div className="c-candidates__form">
                    <form className="c-candidates__form c-candidates__form--input">
                        {/* <input type="checkbox" id="filter" name="filter"/> */}
                        <div></div>
                        <label for="filter">Filtros</label>
                    </form>
                    <p>Ubicación</p>
                    <p>Disponibilidad</p>
                </div>

            </CSSTransition>

            {/* <div className="c-candidates__containers">
                <CandidatesCard/>
                <CandidatesCard/>
                <CandidatesCard/>
                <CandidatesCard/>
                <CandidatesCard/>
            </div> */}

            <CSSTransition
                appear
                in
                classNames='offer-transition'
                timeout={800}
            >

            <div className="c-candidates__gallery">
                {candidates.map((candidate, i) => <CandidatesCard key={i} candidate={candidate}/>) }
            </div>

            </CSSTransition>

            {/* <button className="r-button r-button--fixed"><span className="icon-user"></span></button> */}

            <CSSTransition
                appear
                in
                classNames='navbar-transition'
                timeout={800}
            >
                <Navbar/>
            </CSSTransition>
        </div>
    )
}
