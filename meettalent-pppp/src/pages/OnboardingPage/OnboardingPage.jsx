import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import { FullPageFigure } from '../../shared/components/fullPageFigure/FullPageFigure';

import img1 from '../../assets/images/Ilustraciones/undraw_real_time_collaboration_c62i.svg';
import img2 from '../../assets/images/Ilustraciones/undraw_group_video_el8e.svg'; 
import img3 from '../../assets/images/Ilustraciones/undraw_collaborators_prrw.svg';

import './OnboardingPage.scss';

export function OnboardingPage(){

    const history = useHistory();

    const [page, setPage] = useState(1);
    const totalPages = 4;

    const next = () => {
        setPage(page + 1);
    }

    const join = () => {
        setPage(1);
        history.push('/authentication');
    }

    return(
        
        <CSSTransition
            appear
            in
            classNames='header-transition'
            timeout={800}
        >

            <div className={page === 1 ? "b-dark-theme" : "b-light-theme" }>

                <div className="c-onboarding">
                
                    <header className="c-onboarding__header">
                        <span>
                            {page < totalPages &&    
                                <Link className={page === 1 ? "c-onboarding__link--first" : "c-onboarding__link"} to='/authentication'>saltar</Link>
                            }
                        </span>
                    </header> 

                    <main>  
                        {page === 1 && <FullPageFigure logo={true} text={'Atrae y enamora a los mejores candidatos'}/>}
                        {page === 2 &&
                            <CSSTransition
                                appear
                                in
                                classNames='onBoarding-transition'
                                timeout={300}
                            >
                         <FullPageFigure image={img1} text={'Meettalent es una herramienta que te ayuda a conectar con el mejor talento'}/>
                            </CSSTransition>
                         }
                        {page === 3 &&
                            <CSSTransition
                                appear
                                in
                                classNames='onBoarding-transition'
                                timeout={300}
                            >

                                <FullPageFigure image={img2} text={'Crear eventos únicos para interactuar con los candidatos desde cualquier lugar'}/>

                            </CSSTransition>

                         }
                        {page === 4 && 
                            <CSSTransition
                                appear
                                in
                                classNames='onBoarding-transition'
                                timeout={300}
                            >
                                <FullPageFigure image={img3} text={'Agilizar tus procesos de selección de una manera sencilla, potente y rápida'}/>
                            
                            </CSSTransition>
                        }

                        <div className="c-onboarding__bullets">
                            <div className={page === 1 ? "c-onboarding__bullet c-onboarding__bullet--selected" : "c-onboarding__bullet"}></div>
                            <div className={page === 2 ? "c-onboarding__bullet c-onboarding__bullet--selected" : "c-onboarding__bullet"}></div>
                            <div className={page === 3 ? "c-onboarding__bullet c-onboarding__bullet--selected" : "c-onboarding__bullet"}></div>
                            <div className={page === 4 ? "c-onboarding__bullet c-onboarding__bullet--selected" : "c-onboarding__bullet"}></div>
                        </div>

                        <button className={page === 1 ? "b-button b-button--fixed" : "b-button b-button--inverted b-button--fixed"} onClick={page < totalPages ? next : join}>
                            {page === 2 || page === 3 ? 'Continuar' : (page === 1 ? 'Comenzar' : 'Unirme ahora') }
                        </button>
                        
                    </main>

                </div>

            </div>

        </CSSTransition>
    )
}