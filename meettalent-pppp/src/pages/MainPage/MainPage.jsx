import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import { IsLoggedContext } from '../../shared/contexts/IsLoggedContext';

import logo from '../../assets/images/meettalent\ 2.png';

import './MainPage.scss';

export function MainPage(){

    const {isLogged} = useContext(IsLoggedContext);

    return(
        <CSSTransition
            appear
            in
            classNames='header-transition'
            timeout={800}
        >
            <Link className='c-mainpage' to={!isLogged ? "/onboarding" : "/home" }>
                <img className="c-mainpage__logo" src={logo} alt="Meet Talent logo"/>
            </Link>
        </CSSTransition>
    )
}