import React from 'react';



import './Notifications.scss';



export function Notifications(){
  
    return (
        <div className="c-notifications">
            <div className="c-notifications__container">
                <p className="c-notifications__container__text">Esta semana</p>   
            </div>
            <div className="c-notifications__container__distrobution">
                    <span className="icon-layout c-notifications__container__distrobution--icon"></span>
                    <div className="c-notifications__container__distrobution--text">
                        <p className="c-notifications__container__distrobution--text--title">Hackathon:</p>
                        <p>Dentro de tres días tienes un Hackathon vinculado a la oferta de</p>
                    </div>
                    <span className="icon-more-horizontal c-notifications__container__distrobution--moreHorizontal"></span>
                </div>
                <div className="c-notifications__container__distrobution">
                    <span className="icon-bar-chart c-notifications__container__distrobution--icon"></span>
                    <div className="c-notifications__container__distrobution--text">
                        <p className="c-notifications__container__distrobution--text--title">Resultados Hackathon:</p>
                        <p>Tienes listo el documento de datos recogidos del Hackathon "Puesto</p>
                    </div>
                    <span className="icon-more-horizontal c-notifications__container__distrobution--moreHorizontal"></span>
                </div>
                <div className="c-notifications__container__distrobution">
                    <span className="icon-stack c-notifications__container__distrobution--icon"></span>
                    <div className="c-notifications__container__distrobution--text">
                        <p className="c-notifications__container__distrobution--text--title">Informe optimización:</p>
                        <p>Tienes listo el informe de optimización de la oferta de</p>
                    </div>
                    <span className="icon-more-horizontal c-notifications__container__distrobution--moreHorizontal"></span>
                </div>
                <div className="c-notifications__container__distrobution">
                    <span className="icon-layout c-notifications__container__distrobution--icon"></span>
                    <div className="c-notifications__container__distrobution--text">
                        <p className="c-notifications__container__distrobution--text--title">Caducidad oferta:</p>
                        <p>Dentro de tres días la oferta de diseñador web</p>
                    </div>
                    <span className="icon-more-horizontal c-notifications__container__distrobution--moreHorizontal"></span>
                </div>
                <div className="c-notifications__container">
                    <p className="c-notifications__container__text">Este mes</p>   
                </div>
                <div className="c-notifications__container__distrobution2">
                    <span className="icon-bar-chart c-notifications__container__distrobution2--icon"></span>
                    <div className="c-notifications__container__distrobution2--text">
                        <p className="c-notifications__container__distrobution2--text--title">Resultados Hackathon:</p>
                        <p>Tienes listo el informe de optimización de la oferta de</p>
                    </div>
                    <span className="icon-more-horizontal c-notifications__container__distrobution2--moreHorizontal"></span>
                </div>
                <div className="c-notifications__container__distrobution2">
                    <span className="icon-layout c-notifications__container__distrobution2--icon"></span>
                    <div className="c-notifications__container__distrobution2--text">
                        <p className="c-notifications__container__distrobution2--text--title">Resultados Hackathon:</p>
                        <p>Tienes listo el documento de datos recogidos del Hackathon "Puesto</p>
                    </div>
                    <span className="icon-more-horizontal c-notifications__container__distrobution2--moreHorizontal"></span>
                </div>
                <div className="c-notifications__container__distrobution2">
                    <span className="icon-stack c-notifications__container__distrobution2--icon"></span>
                    <div className="c-notifications__container__distrobution2--text">
                        <p className="c-notifications__container__distrobution2--text--title">Informe optimización:</p>
                        <p>Tienes listo el informe de optimización de la oferta de</p>
                    </div>
                    <span className="icon-more-horizontal c-notifications__container__distrobution2--moreHorizontal"></span>
                </div>
                <div className="c-notifications__container__distrobution2">
                    <span className="icon-layout c-notifications__container__distrobution2--icon"></span>
                    <div className="c-notifications__container__distrobution2--text">
                        <p className="c-notifications__container__distrobution2--text--title">Caducidad oferta:</p>
                        <p>Dentro de tres días la oferta de diseñador web</p>
                    </div>
                    <span className="icon-more-horizontal c-notifications__container__distrobution2--moreHorizontal"></span>
                </div> 
        </div>
    )
}
