import React from 'react';
import { MessageCard } from '../../../shared/components/cards/MessageCard/MessageCard';


import './Messages.scss';



export function TestPagePere(){

    let newMessages= "1";
  
    return (
        <div className="c-messages">
            <div className="c-messages__directory">
                <p>({newMessages}) Recibidos</p>
                <p>Enviados</p>
                <p>Destacados</p>
            </div>
            <MessageCard/>
            <MessageCard/>
            <MessageCard/>

            <button className="r-button r-button--fixed"><span className="icon-edit-3"></span></button>          

        </div>
    )
}
