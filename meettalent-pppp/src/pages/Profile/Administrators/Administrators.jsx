import React from 'react';



import './Administrators.scss';



export function Administrators(){

  
    return (
        <div className="c-administrators">
            <div className="c-administrators--title">
                <p className="c-administrators--title--top">Empresa</p>
                <h2 className="c-administrators--title--company">Nombre de la empresa</h2>
            </div>
            <div className="c-administrators--profile">
                <img src="https://www.nauticalnewstoday.com/wp-content/uploads/2015/10/Tibur%C3%B3n-Duende.jpg" alt="Imagen del usiario Empresa" className="c-administrators--profile--img"/>
                <p>Eduardo Fernández</p>
            </div>
            <div className="c-administrators--add">
                <button className="r-button"><span className="icon-plus-square"></span></button>
                <p className="c-administrators--add__styles">Añade más administradores con la version premium</p>
            </div>
            
        </div>
    )
}
