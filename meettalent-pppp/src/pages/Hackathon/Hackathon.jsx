import React from 'react';
import './Hackathon.scss';


import AnalogClock from 'analog-clock-react';
import {Link} from 'react-router-dom';
import { Checkbox } from './Checkbox/Checkbox';

let options = {
    useCostumTime:true,
    width: "2.5rem",
    border: false,
    borderColor: "none",
    baseColor: "black",
    centerColor: "none",
    centerBorderColor: "none",
    handColors: {
      second: "transparent",
      minute: "var(--color--lightgreen)",
      hour: "var(--color--white)"
    },

// poner un valor predeterminado cuando entres en la pagina, luego cambiarlo al editarlo?
    "seconds": 1,   
    "minutes": 10,  
    "hours": 22    
};


export function Hackathon(){

   
    return (
        <div>
            <header className="b-header">
                <span className="b-header__backIcon">
                    <span className="icon-chevron-left"></span>
                </span> 
                <h2 className="b-header__title">Configuración</h2>
            </header>
            <div className="h-ctop">
                <div className="h-ctop h-ctop__direction">
                    <p className="h-titles-styles">Hora</p>
                    <span className="icon-edit-2 paddingtest"></span>
                </div>
                <div className="h-ctop h-ctop__direction">
                    <AnalogClock {...options}/>
                    <div className="h-ctop h-ctop__direction__order">
                        <h5 className="h-ctop__direction__order--darkblue">Nombre ciudad set value?</h5>
                        <p className="h-ctop__direction__order--grey">Hora de inicio 9:00 set value?</p>
                    </div> 
                </div>                               
            </div>
            <hr className="h-hr-style"></hr>
            <div className="h-ctop padding-top">
                <div className="h-ctop h-ctop__direction">
                    <p className="h-titles-styles">Duración</p>
                    <span className="icon-edit-2"></span>
                </div>
                <div className="h-ctop h-ctop__direction">
                    <div className="h-ctop h-ctop__direction__order">
                        <h5 className="h-ctop__direction__order--darkblue">9.00 hora de inicio set value?</h5>
                        <h5 className="padding-top-h h-ctop__direction__order--darkblue">11.00 hora de fin set value?</h5>
                    </div>
                    <div className="h-ctop h-ctop__direction__order">
                        <div className="ball-style"></div>
                        <hr className="barra-style"></hr>
                        <div className="ball-style"></div>
                    </div>
                    <div className="h-ctop h-ctop__direction__order">
                        <h5 className="h-ctop__direction__order--grey">Inicio</h5>
                        <h5 className="padding-top-h h-ctop__direction__order--grey">Finalización</h5>
                    </div>
                </div>
                <div className="checkbox-position">
                    <p  className="h-titles-styles">Activar recordatorio</p>                  
                    <Checkbox/>
                </div>
            </div>
            {/* link to hackaton home? la url donde va */}
            <Link to=""><button className="b-button b-button--inverted">Continuar</button></Link>                
    </div>            
    )
}
