import React from 'react';
import { Link } from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import './ConstructionPage.scss';

export function ConstructionPage(){
  
    return (
        <CSSTransition
            appear
            in
            classNames='header-transition'
            timeout={800}
        >
        <div className="construction b-main">
                <div className="construction--direction">
                    <div className=" icon icon--pacman"></div>
                    <div className="icon icon--dots"></div>
                    <div className=" icon icon--warning"></div>
                </div>
            
                <p className="construction construction--text">Esta página esta en construcción en este momento, gracias por su paciencia.</p>

                <Link to="/home">
                    <button className="b-button b-button--inverted" >Volver al home</button>
                </Link>
            
            
        </div>
       </CSSTransition>
    )
}
