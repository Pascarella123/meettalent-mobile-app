import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import { OffersContext } from '../../shared/contexts/OffersContext';
import { SearchFilterContext } from '../../shared/contexts/SearchFilterContext';

import {OfferCard} from "../../shared/components/cards/OfferCard/OfferCard";
import {Navbar} from "../../shared/components/Navbar/Navbar";

import './HomePage.scss';

export function HomePage(){

    const {offers} = useContext(OffersContext);
    const {setToFilter, filtered} = useContext(SearchFilterContext);

    const [openOffersCounter,setOpenOffers] = useState(0);

    const counter = () => {
        let counter = 0;
        for(const offer of offers){
            if(!offer.expired){
                counter++;
                setOpenOffers(counter);
            }
        }
    }
    
    useEffect(() => setToFilter({offers: offers}), [offers]);
    useEffect(counter, [offers]);

    return(

        <div className="c-home">

            {filtered && filtered.offers.length > 0 && 
                <>
                    <CSSTransition
                        appear
                        in
                        classNames='header-transition'
                        timeout={800}
                    >
                        <h3>Ofertas abiertas ({openOffersCounter})</h3>
                    </CSSTransition>

                    <CSSTransition
                        appear
                        in
                        classNames='offerHome-transition'
                        timeout={800}
                    >

                        <div className="c-home__gallery">
                            {filtered.offers.map ( (offer, i) =>
                                !offer.expired ? <OfferCard key={i} offer={offer}/> : ''
                            )}
                        </div>

                    </CSSTransition>
                </>
            }

            {!filtered || filtered.offers.length === 0 &&

                <CSSTransition
                    appear
                    in
                    classNames='navbar-transition'
                    timeout={800}
                >

                <div className="c-home__welcome">
                    <h1>Bienvenido a Meettalent</h1>
                    <p>Aún no tienes ninguna oferta creada. Pulsa <Link to='/create/offers'>aquí</Link> para publicar una por primera vez.</p>
                </div>

                </CSSTransition>
            }
            
            
            <CSSTransition
                appear
                in
                classNames='navbar-transition'
                timeout={800}
            >
                <Navbar/>
            </CSSTransition>
        </div>

        
    )
}