import './App.scss';

import React, { useState } from 'react';
import {BrowserRouter as Router} from "react-router-dom";

import { PublicRoutes } from './core/routes/PublicRoutes';
import { PrivateRoutes } from './core/routes/PrivateRoutes';
import { LogInRoutes } from './core/routes/LogInRoutes';
// import { TestRoutes } from './core/TestRoutes/TestRoutes';

import { IsLoggedContext } from './shared/contexts/IsLoggedContext';
import { getCookieUtil } from "./shared/utils/getCookieUtil";

function App() {
  
  const [isLogged, setIsLogged] = useState(!!getCookieUtil('token'));

  return (
      <IsLoggedContext.Provider value={{ isLogged, setIsLogged }}> 
          <Router>

              <PublicRoutes/>

              {!isLogged &&
                <LogInRoutes/>
              }

              {isLogged &&
                <PrivateRoutes/>
              }

            {/* ---BORRAR--- */}
              {/* <TestRoutes/> */}
            {/* ------------ */}

          </Router>
      </IsLoggedContext.Provider>
  );
}

export default App;