import { Route, Switch } from "react-router-dom";

// Pages
import { MainPage } from "../../pages/MainPage/MainPage";
import { PopUpPage } from "../../pages/PopUpPage/PopUpPage";

//Templates
import { TemplateFullPage } from "../../templates/TemplateFullPage/TemplateFullPage";
import { TemplateHeader } from "../../templates/TemplateHeader/TemplateHeader";
import { TemplateHeaderSearch } from "../../templates/TemplateHeaderSearch/TemplateHeaderSearch";


export function PublicRoutes(){

  return(
      <Switch>

          <Route path="/authentication/congrats">
            <TemplateFullPage theme="dark" contHistory="/home" popUpText="Acabas de registrarte en meettalent" textBtn="Comenzar">
              <PopUpPage />
            </TemplateFullPage>
          </Route>

          <Route path="/offer/congrats">
            <TemplateFullPage theme="dark" contHistory="/offers" popUpText="Acabas de publicar una oferta" textBtn="Ver oferta publicada" inOffers={true}>
              <PopUpPage />
            </TemplateFullPage>
          </Route>

          <Route exact path="/">
            <TemplateFullPage theme='dark'>
              <MainPage />
            </TemplateFullPage>
          </Route>
          
      </Switch>
  )
}