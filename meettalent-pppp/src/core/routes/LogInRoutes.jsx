import { Route, Switch } from "react-router-dom";

// Pages
import { LogInPage } from "../../pages/Authentication/LogInPage/LogInPage";
import { RestorePasswordPage } from "../../pages/Authentication/RestorePassword/RestorePassword";
import { RegisterPage } from "../../pages/Authentication/RegisterPage/RegisterPage";
import { OnboardingPage } from "../../pages/OnboardingPage/OnboardingPage";

//Templates
import { TemplateHeader } from "../../templates/TemplateHeader/TemplateHeader";
import { TemplateFullPage } from "../../templates/TemplateFullPage/TemplateFullPage";


export function LogInRoutes(){

  return(
      <Switch>

          <Route path="/authentication/restore-password">
            <TemplateHeader title='Restablecer contraseña' backHistory='/authentication' theme='img' center={true}>
              <RestorePasswordPage />
            </TemplateHeader>
          </Route>

          <Route path="/authentication/register">
            <TemplateHeader title='Crear cuenta' backHistory='/authentication' theme='light'>
              <RegisterPage />
            </TemplateHeader>
          </Route>
          
          <Route path="/authentication/">
            <TemplateFullPage theme="dark">
              <LogInPage />
            </TemplateFullPage>
          </Route>

          <Route path="/onboarding">
              <OnboardingPage />
          </Route>
          
      </Switch>
  )
}