import { Route, withRouter, Switch } from "react-router-dom";
import { useState } from "react";
// import { CSSTransition, TransitionGroup } from "react-transition-group";

// Pages
import { CreatePage } from "../../pages/CreatePage/CreatePage";
import { HomePage } from '../../pages/HomePage/HomePage';
import { OfferDescription } from "../../pages/CreateOffer/OfferDescription/OfferDescription";
import { PublishedOffers } from '../../pages/CreateOffer/PublishedOffers/PublishedOffers';
import { Offers } from "../../pages/Offers/Offers";
import { ConstructionPage } from "../../pages/ConstructionPage/ConstructionPage";
import { Candidates1 } from "../../pages/Candidates/Candidates1";
import { CandidatesInfo } from "../../pages/Candidates/CandidatesInfo/CandidatesInfo";

//Templates
import { TemplateHeader } from "../../templates/TemplateHeader/TemplateHeader";
import { TemplateHeaderSearch } from "../../templates/TemplateHeaderSearch/TemplateHeaderSearch";


export function PrivateRoutes(){

  const [title, setTitle] = useState();

  return(
      <Switch>

          <Route path="/create/offers">
            <TemplateHeader title='Descripción de la Oferta' backHistory='/create' theme='light' mainRound={true}>
              <PublishedOffers />
            </TemplateHeader>
          </Route>

          <Route path="/create/offer/:offerId">
            <TemplateHeader title={title} setTitle={setTitle} backHistory='/create/offers' theme='light'>
              <OfferDescription />
            </TemplateHeader>
          </Route>

          <Route path="/create/offer">
            <TemplateHeader title={title} setTitle={setTitle} backHistory='/create/offers' theme='light'>
              <OfferDescription />
            </TemplateHeader>
          </Route>

          <Route exact path="/create">
            <TemplateHeader theme='light' headerImg={true} opacity={true} mainRound={true}>
              <CreatePage />
            </TemplateHeader>
          </Route>

          <Route path="/notifications">
            <ConstructionPage/>
          </Route>

          <Route path="/create/hackathon">
            <ConstructionPage/>
          </Route>

          <Route path="/offers">
            <TemplateHeader title='Ofertas' backHistory='/home' theme='light' mainRound={true} >
              <Offers />
            </TemplateHeader>
          </Route>

          <Route path="/candidates">
            <TemplateHeader title='Candidatos' backHistory='/home' theme='light' mainRound={true} >
              <Candidates1 />
            </TemplateHeader>
          </Route>

          <Route path="/home">
            <TemplateHeaderSearch theme='light' headerImg={true} userAndChat={true}>
              <HomePage />
            </TemplateHeaderSearch>
          </Route>
          
      </Switch>
  )
}