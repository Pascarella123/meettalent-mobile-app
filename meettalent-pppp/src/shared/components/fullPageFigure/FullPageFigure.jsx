import React from 'react';

import logoImg from '../../../assets/images/meettalent-final.png';
import textLogo from '../../../assets/images/letras_sueltas.png';

import './FullPageFigure.scss';

export function FullPageFigure({image, text, logo, popup, title}){

    return(
        <div className="c-fullPageFigure"> 
            {logo &&
                <div className="c-fullPageFigure__logoContainer">
                    <img className="c-fullPageFigure__logo" src={logoImg} alt="MeetTalent logo"/>
                    <h3>Nos encanta verte por</h3>
                    <img className="c-fullPageFigure__textLogo" src={textLogo} alt="MeetTalent text logo"/>
                </div>
            }
            {!logo &&
                <img className={popup ? "c-fullPageFigure__illustration c-fullPageFigure__illustration--popup" : "c-fullPageFigure__illustration"} src={image} alt="illustration"/>
            }
            {popup && 
                <h1 className="c-onboarding-slide__title">{title}</h1>
            }
            <p className={popup ? "c-fullPageFigure__text c-fullPageFigure__text--popup" : "c-fullPageFigure__text"}>{text}</p>
        </div>
    )
}