import React from 'react';
import './CandidatesCard.scss';

export function CandidatesCard({candidate}) {

    return (
        <div className="c-candidateCard">
            <img className="c-candidateCard__image" src={candidate.image ? candidate.image : "https://d500.epimg.net/cincodias/imagenes/2016/07/04/lifestyle/1467646262_522853_1467646344_noticia_normal.jpg"} alt="candidate image"/>
            <div className="c-candidateCard__container">
                <h3 className="c-candidateCard__name">{candidate.name}</h3>
                <h4 className="c-candidateCard__profession">{candidate.role}</h4>

                <hr className="c-candidateCard__separator"></hr>

                <div className="c-candidateCard__data">
                    <div>{candidate.age+' años'}</div>
                    <div><span className="icon-map-pin"></span>{' '+candidate.direction.city}</div>
                </div>
            </div>
        </div>
    )
}