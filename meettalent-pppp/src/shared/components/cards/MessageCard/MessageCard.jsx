import React from 'react';


import './MessageCard.scss';



export function MessageCard(){

    let string = "Sería un placer poder ampliar la información sobre mi formación, experiencia y habilidades ESTO ES PARA VER SI CORTA EL TEXTO O NO. ME GUSTA LA PIZZA Y MUCHAS OTRAS COSAS BONITAS";

    let result = string.substring(0,90);

    let textPost = result + " ...";
   
    return (
        <div className="m-userMessage">
            <img src="https://www.renovablesverdes.com/wp-content/uploads/2019/05/pez-gota.jpg" alt="Imagen del candidato" className="m-userMessage__picture"/>
            <div className="m-userMessage__chat">
                <h4 className="m-userMessage__chat--title">Nilton García</h4>
                <p className="m-userMessage__chat--p element">{textPost}</p>
            </div>
            

        </div>
    )
}
