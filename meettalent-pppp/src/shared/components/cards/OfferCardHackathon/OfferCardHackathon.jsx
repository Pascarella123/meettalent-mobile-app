import React, { useState } from 'react';

import { ProcessBar } from '../OfferCard/components/ProcessBar/ProcessBar';

import '../OfferCard/OfferCard.scss';



export function OfferCardHackathon({offer}){

    // CHECK SI CHECK NO

    const [check, setCheck]= useState(false);


    const showCheck =() =>{
        check? setCheck(false) : setCheck(true);
        
    }

    // para poder coger fechas

 
   
    return (
        <div className="c-offercard">
        <div className="c-offercard__infoDetail">
            <span className="c-offercard__infoDetail__span">20/02/2021</span>
            <div onClick={showCheck} className={check?"check-square check-square__span" : "check-square check-square__nospan"}>
                <span className="icon-check"></span>
            </div>
        </div>
        <h3>Diseñador Web</h3>
        <div className="c-offercard__mainInfo">
            <div className="c-offercard__mainInfo__box">
                <span className="icon-home"></span>
                <p>Madrid</p>
            </div>
            <div className="c-offercard__mainInfo__box">
                <p>2</p>
                <span className="icon-user"></span>
            </div>
            <div className="c-offercard__mainInfo__box">
                <span className="icon-eye-off"></span>
                <p>27/02/2021</p>
            </div>
        </div>
        <hr></hr>
        <div className="c-offercard__lowOffer2">
            <div className="partition_hackaton-box">
                <div className="c-offercard__lowOffer c-offercard__lowOffer2--direction">
                    <h5>Proceso</h5>
                    <h5>80%</h5>                          
                </div>
                <ProcessBar percentage={80}/>     
            </div>
            <div className="c-offercard__lowOffer c-offercard__lowOffer2--direction partition_hackaton-box2">
                <div>
                    <p>43</p>
                    <h5>En proceso</h5>
                </div>
            </div>
        </div>              
    </div>             
    )
}
