import React from 'react';
import { ProcessBar } from './components/ProcessBar/ProcessBar';
import './OfferCard.scss';

export function OfferCard({offer}){
    
    //-----------BORRAR ESTA PARIDA CUANDO TOQUE --------------------------
    const random = (min, max) => {
        return Math.trunc(Math.floor((Math.random() * (max - min + 1)) + min));
    };
    const ins = random(5, 100);
    const pro = random(5, ins - 1);
    const fin = ins-pro;

    const inscriptions = {
        number: ins,
        process: pro,
        finalist: fin
    };
    //----------------------------------------------------------------------

    const createdAt = offer.createdAt;
    const expireAt = offer.expireAt;

    const toDate = (date, format = 'string') => {
        const year = date.substring(0, 4);
        const month = date.substring(5, 7);
        const day = date.substring(8, 10);
        const hour = date.substring(11, 13);
        const min = date.substring(14, 16);
        const sec = date.substring(17, 19);
        const mil = date.substring(20, 22)
        
        const operFormated = year + '-' + month + '-' + day + 'T' + hour + ':' + min + ':' + sec + '.' + mil + 'Z';
        const dateFormated = new Date(operFormated);
        const stringFormated = dateFormated.toLocaleDateString();
        if (format === 'string') {
            return stringFormated;
        } else if (format === 'date') {
            return dateFormated;
        } else if (format === 'operation') {
            return operFormated;
        } else {
            return null;
        }
    }

    const processPercentage = () => {
        const total = toDate(expireAt, 'date') - toDate(createdAt, 'date');
        const spent = new Date() - toDate(createdAt, 'date');
        const percentage = (spent / total) * 100;
        if (percentage < 100) {
            return Math.trunc(percentage);
        } else {
            return 100;
        }
    }
    
    return (
        <div className="c-offercard">
            <div className="c-offercard__infoDetail">
                <span className="c-offercard__date">{toDate(createdAt, 'string')}</span>
                <span className={processPercentage() < 100 ? "c-offercard__lock icon-unlock" : "c-offercard__lock icon-lock"}></span>
            </div>
            <h3 className="c-offercard__title">{offer.title}</h3>
            <div className="c-offercard__mainInfo">
                <div className="c-offercard__mainInfo__box">
                    <span className="icon-map-pin"></span>
                    <p>{offer.city}</p>
                </div>
                <div className="c-offercard__mainInfo__box">
                    <p>{offer.vacant}</p>
                    <span className="icon-user"></span>
                </div>
                <div className="c-offercard__mainInfo__box">
                    <span className="icon-eye-off"></span>
                    <p>{toDate(offer.expireAt)}</p>
                </div>
            </div>
            <hr/>
            <div className="c-offercard__lowOffer">
            {!offer.expired
            ? <>
                <div className="c-offercard__lowOffer c-offercard__lowOffer--direction">
                    <h5>Proceso</h5>
                    <h3>{processPercentage() + '%'}</h3>                          
                </div>
                <ProcessBar percentage={processPercentage()}/>
            </>
            : <div className="c-offercard__lowOffer c-offercard__lowOffer--direction c-offercard__lowOffer--direction--completedOffer"> 
                {/* ¡¿Qué estilos son estos, por qué mod de mod?! */}
                <span className="icon-check"></span>
                <h5>Proceso finalizado</h5>
            </div>}
                
                <div className="c-offercard__lowOffer c-offercard__lowOffer--direction">
                    <div className="c-offercard__lowOffer__item">
                        <p>{inscriptions.number}</p>
                        <h6>Inscritos</h6>
                    </div>
                    <div className="c-offercard__lowOffer__item">
                        <p>{inscriptions.process}</p>
                        <h6>En proceso</h6>
                    </div>
                    <div className="c-offercard__lowOffer__item">
                        <p>{inscriptions.finalist}</p>
                        <h6>Finalistas</h6>
                    </div>
                </div>
            </div>              
        </div>
    )
}