import React, { useState } from 'react';
import './ProcessBar.scss';


export function ProcessBar({percentage}){   

    return (       
            <div className="progress-bar">
                <div className="filler" style={{width: percentage +"%"}}></div>          
            </div>            
        
    )
}