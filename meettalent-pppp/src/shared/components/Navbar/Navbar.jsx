import React from 'react';
import { NavLink } from 'react-router-dom';
import { Badge } from '../littleComponents/Badge/Badge';
import './Navbar.scss';

export function Navbar(){

    return (
        <div className="c-navbar">
            <NavLink to="/home" className="c-navbar__iconBlock" activeClassName="c-navbar__iconBlock--active">
                <span className="c-navbar__iconBlock__span icon-home"></span>
                <p className="c-navbar__iconBlock__text">Home</p>
            </NavLink>
            <NavLink to="/candidates" className="c-navbar__iconBlock" activeClassName="c-navbar__iconBlock--active">
                <span className="c-navbar__iconBlock__span icon-users"></span>
                <p className="c-navbar__iconBlock__text">Candidatos</p>
            </NavLink>
            <NavLink to="/create" className="c-navbar__iconBlock" activeClassName="c-navbar__iconBlock--active">
                <span className="c-navbar__iconBlock__span icon-plus-square"></span>
                <p className="c-navbar__iconBlock__text">Crear</p>
            </NavLink>
            <NavLink to="/notifications" className="c-navbar__iconBlock" activeClassName="c-navbar__iconBlock--active">
                <Badge number={0}>
                    <span className="c-navbar__iconBlock__span icon-bell"></span>
                </Badge>
                <p className="c-navbar__iconBlock__text">Notificaciones</p>
            </NavLink>
            <NavLink to="/offers" className="c-navbar__iconBlock" activeClassName="c-navbar__iconBlock--active">
                <span className="c-navbar__iconBlock__span icon-briefcase"></span>
                <p className="c-navbar__iconBlock__text">Ofertas</p>
            </NavLink>
            
        </div>
    )
}