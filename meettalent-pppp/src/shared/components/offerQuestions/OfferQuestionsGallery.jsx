import React from 'react';
import './OfferQuestionsGallery.scss';

export function OfferQuestionsGallery({questions, fnRemoveQuestion}) {

    return (
        <>
            {questions && <label className="b-label b-label__sectionTitle">Preguntas</label>}
            <ol className="c-offerQuestionsGallery">
                {questions.map((item, i) => 
                    <li className="c-offerQuestionsGallery__question" key={i}>
                        <div className="c-offerQuestionsGallery__question__title">
                            <span>{item.title}</span><span data-index={i} className="icon-trash-2" onClick={fnRemoveQuestion}></span>
                        </div>
                        {item.answers.map((answer, ii) => 
                            <div className="c-offerQuestionsGallery__question__answers" key={ii}>
                                <span className="c-offerQuestionsGallery__question__answer__item c-offerQuestionsGallery__question__answers__item c-offerQuestionsGallery__question__answers__item--answer">{answer.answer}</span>
                                {answer.textLength !== undefined
                                    ? <span className="c-offerQuestionsGallery__question__answer__item c-offerQuestionsGallery__question__answers__item c-offerQuestionsGallery__question__answers__item--score">{answer.textLength ? answer.textLength + " letras" : null}</span>
                                    : <span className="c-offerQuestionsGallery__question__answer__item c-offerQuestionsGallery__question__answers__item c-offerQuestionsGallery__question__answers__item--score">{answer.score !== '' ? answer.score + " puntos" : null}</span>
                                }
                            </div>
                        )}
                    </li>
                )}
            </ol>
        </>
    )
}