import React from 'react';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import './AddOfferQuestions.scss';

export function AddOfferQuestions({fnAddQuestion, fnQuestions}){

    // lista de preguntas:
    // const newQuestion = document.getElementById('questions').value;

    const {register,handleSubmit} = useForm();

    const [newQuestionTitle, setNewQuestionTitle] = useState();
    const [newQuestionType, setNewQuestionType] = useState();
    const [newAnswers, setNewAnswers] = useState([]);
    const [showAlert, setShowAlert] = useState(false);
    const [showSubmit, setShowSubmit] = useState(false);

    const createNewQuestion = () => {
        const newTitle = document.getElementById('newQuestion').value;
        if (newTitle !== ''){
            setNewQuestionTitle(newTitle);
            // console.log(newQuestion);
        }
        // reset();
    }

    const changeNewQuestionType = (typeOfAnswers) => {
        //resetea los campos anteriormente rellenos
        const answersValues = document.querySelectorAll(".c-addOfferQuestions__answers__item--score");
        for (const item of answersValues) {
            // console.log(item);
            item.value = '';
        }
        setNewQuestionType(typeOfAnswers);

        //establece valores por defecto para respuestas preestablecidas
        if (typeOfAnswers === "score") {
            setNewAnswers([])
        }else if (typeOfAnswers === "text") {
            setNewAnswers([{answer: "Longitud máxima de texto: ",textLength: 450, score: 0}])
        }else if (typeOfAnswers === "boolean") {
            setNewAnswers([{answer: "Si", score: ''}, {answer: "No", score: ''}])
        }

    }

    const addAnswer = () => {
        if (newQuestionType === "score") {
            let $$newQuestionAnswer = document.getElementById("newQuestionAnswer");
            const answer =  $$newQuestionAnswer.value;

            let $$newQuestionAnswerScore = document.getElementById("newQuestionAnswerScore");
            const score =  $$newQuestionAnswerScore.value;

            let wrong = false;
            if (score > 99 || score < -99 || score === '' || answer === '') {
                wrong = true
            } else {
                for (const item of newAnswers) {
                    if (item.answer.toLowerCase() === answer.toLowerCase()) {
                        wrong = true
                    }
                }
            }

            if (!wrong) {
                $$newQuestionAnswer.value = '';
                $$newQuestionAnswerScore.value = '';
                const currentAnswers = ([...newAnswers, {answer: answer, score: score}]);
                setNewAnswers(currentAnswers);
                setShowAlert(false);
            } else {
                setShowAlert(true);
            }
        } else if (newQuestionType === "text"){
            let $$newQuestionAnswerLength = document.getElementById("newQuestionAnswerLength");
            const answerLength =  $$newQuestionAnswerLength.value;

            const currentAnswers = ([{answer: "Longitud máxima de texto: ",textLength: answerLength, score: 0}]);
            setNewAnswers(currentAnswers);

            let wrong = false;
            if (answerLength <= 0) {
                wrong = true
            }
            if (wrong) {
                setShowAlert(true);
            } else {
                setShowAlert(false);
            }

        } else if (newQuestionType === "boolean") {
            let $$newQuestionAnswerScoreYes = document.getElementById("newQuestionAnswerScoreYes");
            let scoreYes =  $$newQuestionAnswerScoreYes.value;
            let $$newQuestionAnswerScoreNo = document.getElementById("newQuestionAnswerScoreNo");
            let scoreNo =  $$newQuestionAnswerScoreNo.value;

            const currentAnswers = ([{answer: "Si", score: scoreYes}, {answer: "No", score: scoreNo}]);
            setNewAnswers(currentAnswers);

            let wrong = false;
            if (scoreYes === "" || scoreNo === "") {
                wrong = true
            } else if (scoreYes > 99 || scoreNo > 99 || scoreYes < -99 || scoreNo < -99){
                wrong = true;
            }


            if (wrong) {
                setShowAlert(true);
            } else {
                setShowAlert(false);
            }
            
        }
        
    }

    const removeNewAnser = (event) => {
        //consigue el atributo "index" del elemento que lanza el evento
        const indexToRemove = event.target.dataset.index;
        const currentAnswers = [...newAnswers];
        currentAnswers.splice(indexToRemove, 1);
        // console.log(currentAnswers);
        setNewAnswers(currentAnswers);
        setShowAlert(false);
    }

    const showSubmitButton = () => {
        if (newQuestionType === "score" && newAnswers.length > 0) {
            // console.log('score');
            // console.log(newAnswers);
            setShowSubmit(true);
        } else if (newQuestionType === "text" && newAnswers[0].textLength > 0 && newAnswers[0].textLength !== '') {
            // console.log('text');
            // console.log(newAnswers);
            setShowSubmit(true);
        } else if (newQuestionType === "boolean" && newAnswers[0].score !== '' && newAnswers[1].score !== '' && newAnswers[0].score >= -99 && newAnswers[0].score <= 99 && newAnswers[1].score >= -99 && newAnswers[1].score <= 99) {
            // console.log('boolean');
            // console.log(newAnswers[0].score);
            // console.log(newAnswers[1].score);
            setShowSubmit(true);
        } else {
            setShowSubmit(false)
        };
            
    }


    const addNewQuestion = () => {
        const newQuestion = {title: newQuestionTitle, type: newQuestionType, answers: newAnswers}
        
        fnAddQuestion(newQuestion);
        document.getElementById('newQuestion').value = '';
        setNewQuestionTitle();
        setNewQuestionType();
        setNewAnswers([]);
    }

    // useEffect(() => console.log(newQuestionTitle), [newQuestionTitle]);
    // useEffect(() => console.log(newQuestionType), [newQuestionType]);
    // useEffect(() => console.log(newAnswers), [newAnswers]);
    // useEffect(() => console.log(newAnswers.length), [newAnswers]);
    // useEffect(() => setShowAlert(false), [newAnswers]);
    useEffect(showSubmitButton, [newAnswers]);

    return(
        <>
            {/* <label htmlFor="newQuestions" className="b-label">Añadir pregunta</label> */}
            <div className="c-addOfferQuestions__addQuestion">
                <input className="b-input b-input--white" name="newQuestion" type="text" id="newQuestion" placeholder="Añadir pregunta" ref={register}></input>
                {!newQuestionTitle ? <span className="icon-plus-square c-addOfferQuestions__addQuestion__icon" onClick={createNewQuestion}></span> : <span className="icon-edit-3 c-addOfferQuestions__addQuestion__icon" onClick={createNewQuestion}></span>}
            </div>
            
            {newQuestionTitle &&
            <>
                <h3 className="b-label">{newQuestionTitle}</h3>
                <select className="b-select b-select--white" type="text" id="newQuestionType" name="newQuestionType" ref={register} onChange={handleSubmit(data => changeNewQuestionType(data.newQuestionType))}>
                    <option hidden>Seleccione tipo de respuesta</option>
                    <option value="score">Tipo puntuación</option>
                    <option value="text">Tipo texto</option>
                    <option value="boolean">Tipo Si/No</option>
                </select>
            </>}

            {newQuestionType === "score" ?
                <>
                    {newAnswers && 
                        <div className="c-addOfferQuestions__answers">
                            <p className="c-addOfferQuestions__answers__header c-addOfferQuestions__answers__header--answer">Respuesta</p>
                            <p className="c-addOfferQuestions__answers__header c-addOfferQuestions__answers__header--score">Puntuación</p>
                        </div>
                    }

                    {newAnswers && newAnswers.map((item, i) => 
                        <div key={i} className="c-addOfferQuestions__answers">
                            <p className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--answer">{item.answer}</p>
                            <p className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--score">{item.score} puntos</p>
                            <span className="icon-trash-2 c-addOfferQuestions__answers__icon" data-index={i} onClick={removeNewAnser}></span>
                        </div>
                    )}

                    <div className="c-addOfferQuestions__answers c-addOfferQuestions__answers--newAnswer">
                        <input className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--answer" type="text" id="newQuestionAnswer" name="newQuestionAnswer" placeholder="Respuesta"></input>
                        <input className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--score" type="number" max={99} id="newQuestionAnswerScore" name="newQuestionAnswerScore" placeholder="Puntuación"></input>
                    </div>
                    {showAlert && <p className="c-addOfferQuestions__warning">Las respuestas deben ser distintas, y las puntuaciones deben estar comprendidas entre -99 y 99 puntos</p>}
                    <p className="b-round-sec-btn__button" onClick={addAnswer}>Confirmar respuesta</p>
                </>

            : newQuestionType === "text" ?
                <>
                    <div className="c-addOfferQuestions__answers c-addOfferQuestions__answers--newAnswer">
                        <p className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--answer">Longitud máxima del texto</p>
                        <input className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--score" type="number" id="newQuestionAnswerLength" name="newQuestionAnswerLength" defaultValue={450} onChange={addAnswer} placeholder="Long. máxima"></input>
                    </div>

                    {showAlert && <p className="c-addOfferQuestions__warning">La longitud del texto no puede ser cero ni negativa</p>}
                </>

            : newQuestionType === "boolean" ?
                <>
                    <div className="c-addOfferQuestions__answers">
                        <p className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--answer">SI</p>
                        <input className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--score" type="number" max={99} id="newQuestionAnswerScoreYes" name="newQuestionAnswerScoreYes" placeholder="Puntuación Si" onChange={addAnswer}></input>
                    </div>
                    <div className="c-addOfferQuestions__answers">
                        <p className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--answer">NO</p>
                        <input className="c-addOfferQuestions__answers__item c-addOfferQuestions__answers__item--score" type="number" max={99} id="newQuestionAnswerScoreNo" name="newQuestionAnswerScoreNo" placeholder="Puntuación No" onChange={addAnswer}></input>                            
                    </div>

                    {showAlert && <p className="c-addOfferQuestions__warning">Las puntuaciones deben estar comprendidas entre -99 y 99 puntos</p>}
                </>
                
            : <></>}

            {showSubmit ? <button className="b-round-sec-btn__button" onClick={addNewQuestion}>Añadir respuestas al formulario</button> : <></>}
        </>
    )
}