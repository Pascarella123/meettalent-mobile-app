import React from 'react';

import './Header.scss';

import logo from '../../../assets/images/letrassueltas.png';

export function Header({title, back, headerImg, opacity, leftFn, rightFn, showClose}){

    return(
            
        <header className="c-header">
            <span className="c-header__leftIcon" onClick={leftFn}>
                {back &&
                    <span className="icon-chevron-left"></span>
                }
            </span> 

            {!headerImg &&
                <h2 className="c-header__title">{title}</h2>
            }

            {headerImg &&
                <img className={opacity ? "c-header__title c-header__title--img c-header__title--translucid":"c-header__title c-header__title--img"} src={logo} alt="logo Meettalent"/>
            }

            <span className="c-header__rightIcon" onClick={rightFn}>
                {showClose &&
                    <span className="icon-x"></span>
                }
            </span>
        </header>

    )
}