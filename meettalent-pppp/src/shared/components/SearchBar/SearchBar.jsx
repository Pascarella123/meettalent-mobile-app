import React, { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { KeywordsGallery } from '../littleComponents/KeywordsGallery/KeywordsGallery';
import { CSSTransition } from 'react-transition-group';
import './SearchBar.scss';


export function SearchBar({userAndChat, setFiltered, toFilter, inOffers}){

    const [showMenu, setShowMenu] = useState(false);
    const [filterApplied, setFilterApplied] = useState(false);
    const [keywords, setKeywords] = useState([]);
    const history = useHistory();

    const {register, handleSubmit, reset} = useForm();

    const search = (filter) => {
        let filteredData = {};
        for (const family in toFilter) {
            const results = [];
            if (family === 'offers') {
                for (const item of toFilter[family]) {
                    if (typeof filter === 'string') {
                        setFilterApplied(false);
                        if (filter === '' || item.title.toLowerCase().includes(filter.toLowerCase())) {
                            results.push(item);
                        }
                    } else {
                        // console.log('filtrado personalizado');
                        if (filter.filterAndOr === 'and') {
                            if (item.title.toLowerCase().includes(filter.title.toLowerCase()) || filter.title === '') {
                                // if (item.expired === filter.expired || filter.expired === '' || item.expired === undefined) {
                                    let keywordsCoincidence = false;
                                    if (item.keywords.length === 0 && filter.keywords.length === 0) {
                                        keywordsCoincidence = true;
                                    } else {
                                        let coincidences = 0;
                                        for (const filterKeyword of filter.keywords) {
                                            for (const keyword of item.keywords) {
                                                if (keyword.toLowerCase().includes(filterKeyword.toLowerCase())) {
                                                    coincidences++;
                                                }
                                            }
                                        }
                                        if (filter.keywords.length === coincidences) {
                                            keywordsCoincidence = true;
                                        }
                                    }
                                    if (keywordsCoincidence) {
                                        results.push(item);
                                    }
                                // }
                            }
                        } else if (filter.filterAndOr === 'or') {
                            let pass = false;
                            if (item.title.toLowerCase().includes(filter.title.toLowerCase())) {
                                pass = true;
                            // }else if (item.expired === filter.expired) {
                            //     pass = true;
                            } else {
                                for (const filterKeyword of filter.keywords) {
                                    for (const keyword of item.keywords) {
                                        if (keyword.toLowerCase().includes(filterKeyword.toLowerCase())) {
                                            pass = true;
                                        }
                                    }
                                }
                            }
                            if (pass) {
                                results.push(item);
                            }
                        }
                    }
                }
            }
            filteredData[family] = results;
        }
        setFiltered(filteredData);
    }

    const changeShowMenu = () => {
        showMenu ? setShowMenu(false) : setShowMenu(true);
    }


    const addKeyword = (word) => {
        if (word !== '' && !keywords.includes(word)) {
            const currentKeywords = [...keywords, word];
            setKeywords(currentKeywords);
            document.getElementById("keywords").value = '';
        }else if (keywords.includes(word)){
            document.getElementById("keywords").value = '';
        }
    }
    const removeKeyword = (event) => {
        const currentKeywords = [...keywords];
        currentKeywords.splice(currentKeywords.indexOf(event.target.textContent), 1);
        setKeywords(currentKeywords);
    }


    const includeFilters = (data) => {
        let newFilterOptions = {};
        document.getElementById('search').value = '';
        for (const name in data) {
            const including = {...newFilterOptions, [name]: data[name]};
            newFilterOptions = {...including};
        }
        let includingKeywords = {...newFilterOptions, keywords: keywords}
        newFilterOptions = includingKeywords;
        search(newFilterOptions);
        setShowMenu(false);
    }

    const resetFilters = () => {
        reset();
        setKeywords([]);
        search("");
        setFilterApplied(false);
        setShowMenu(false);
    }

    useEffect(() => setFiltered(toFilter), [toFilter]);
   
    return (
        <>
            <div className={inOffers ? "c-search c-search--inOffers" : "c-search"}>
                {userAndChat && 
                    <div className="c-search__img">
                        <img className="b-img" src="https://www.researchgate.net/profile/Maria-Monreal/publication/315108532/figure/fig1/AS:472492935520261@1489662502634/Figura-2-Avatar-que-aparece-por-defecto-en-Facebook.png" alt="foto del usuario"/>
                    </div>
                }

                <div className="c-search__searchbar">
                    <form className="c-search__form">
                        <label htmlFor="search" ><span className="icon-search"></span></label>
                        <input type="text" name="search" id="search" placeholder="Buscar" size="10" onChange={(e) => search(e.target.value)}/>
                    </form>
                    <span className={!filterApplied ? "icon-equalizer c-search__spanform" : "icon-equalizer c-search__spanform c-search__spanform--blue"} onClick={changeShowMenu}></span>
                </div>

                {userAndChat && <span className="c-search__chatLink icon-messages size"></span>}
            
            </div>
                {!inOffers &&
                <CSSTransition
                    in={showMenu}
                    timeout={800}
                    // classNames="c-search__menu"
                    // unmountOnExit
                    onEnter={() => console.log('entra')}
                    onExited={() => console.log('sale')}
                >
                <div className={showMenu ? "c-search__menu" : "c-search__menu c-search__menu--hidden"}>
                {/* <div className="c-search__menu"> */}
                    {toFilter && toFilter.offers &&
                    <>
                        <p className="b-label b-label--bold">Filtrado de ofertas</p>
                        <form onSubmit={handleSubmit(includeFilters)}>

                            <label className="b-label" htmlFor="filterAndOr">Oferta expirada</label>
                            <select className="b-select b-select--white" name="filterAndOr" type="select" id="filterAndOr" ref={register} defaultValue="">
                                <option value={'and'}>Incluir todos los filtros</option>
                                <option value={'or'}>Incluir al menos un filtro</option>
                            </select>

                            <label className="b-label" htmlFor="title">Título:</label>
                            <input className="b-input b-input--white" type="text" id="title" name="title" placeholder="Escriba el título" ref={register}/>

                            {/* <label className="b-label" htmlFor="expired">Oferta expirada</label>
                            <select className="b-select b-select--white" name="expired" type="select" id="expired" ref={register}>
                                <option hidden value=''>Seleccione una opción</option>
                                <option value={true}>Si (ofertas cerradas)</option>
                                <option value={false}>No (ofertas abiertas)</option>
                            </select> */}

                            <label className="b-label" htmlFor="keywords">Codificaciones internas</label>
                            <div className="keywordsInputContainer">
                                <input className="b-input b-input--white" name="keywords" type="text" id="keywords" placeholder="Añadir palabras clave"></input>
                                <span className="icon-plus-square keywordsInputContainer__addKeyword" onClick={() => addKeyword(document.getElementById("keywords").value)}></span>
                            </div>
                            <KeywordsGallery list={keywords} fnRemove={removeKeyword}/>
                            <div className="b-round-sec-btn__container">
                                <button className="b-round-sec-btn__button" type="submit" onClick={() => setFilterApplied(true)}>Aplicar filtros<span className="icon-check b-round-sec-btn__button__icon"></span></button>
                                <button className="b-round-sec-btn__button" onClick={resetFilters}>Borrar filtros<span className="icon-x icon-check b-round-sec-btn__button__icon b-round-sec-btn__button__icon--red"></span></button>
                            </div>
                        </form>
                    </>}

                    {toFilter && toFilter.candidates && <>
                        <p className="b-label">Candidatos/perfiles</p>
                    </>}

                    {toFilter && toFilter.hackathons && <>
                        <p className="b-label">Pruebas realizadas</p>
                    </>}

                    {!toFilter &&
                    <>
                        <p className="b-label" onClick={() => history.push('/offers')}>Ofertas</p>
                        <p className="b-label" onClick={() => history.push('/candidates')}>Candidatos/perfiles</p>
                        <p className="b-label" onClick={() => history.push('/hackathons')}>Pruebas realizadas</p>
                    </>}
                </div>
            </CSSTransition>
            }
        </>
    )
}