import React from 'react';
import './KeywordsGallery.scss';

export function KeywordsGallery({list, fnRemove}){
    return (
        <div className="c-keywordsGallery">
            {list.map((word, i) => i % 2 === 0 ? <p onClick={fnRemove} className="c-keywordsGallery__item" key={i}>{word}</p> : <p onClick={fnRemove} className="c-keywordsGallery__item c-keywordsGallery__item--inverted" key={i}>{word}</p>)}
        </div>
    )
}