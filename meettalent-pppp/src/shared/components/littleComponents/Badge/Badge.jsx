import React from 'react';
import './Badge.scss';

export function Badge({ children, number }){

    let print = false;

    if (number && number >= 0) {
        print = true;
    }

    return (
        <>
            <div className="c-badge">
                {children}
                {print && <span className="c-badge__notification">{number <= 99 ? number : '+99'}</span>}
            </div>
        </>
    )
}