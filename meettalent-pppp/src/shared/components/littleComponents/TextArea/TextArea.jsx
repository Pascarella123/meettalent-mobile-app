import React, { useState } from 'react';
import './TextArea.scss'

export function TextArea({fnRef, fnId, fnName, fnPlaceholder}){

    const [charNumber, setCharNumber] = useState(0);

    const actualizeCharNumber = (event) => {
        setCharNumber(event.target.value.length);
    }
    
    return (
        <>
            <div className="c-textArea">
                <textarea onInput={actualizeCharNumber} ref={fnRef} className="c-textArea__field" id={fnId} name={fnName} rows="9" placeholder={fnPlaceholder} maxLength="450"></textarea>
                <p className="c-textArea__charNumber">{charNumber}/450</p>
            </div>
        </>
    )
}