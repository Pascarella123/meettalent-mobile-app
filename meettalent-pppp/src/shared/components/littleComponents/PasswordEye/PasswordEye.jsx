import React from 'react';
import { useState } from 'react';
import './PasswordEye.scss';

//ADMITE PROPS: *props.forId *props.children
export function PasswordEye(props){

  const [hidden, setHidden] = useState(true);

    function myFunction() {
        var x = document.getElementById(props.forId);
        // console.log(props.forId)
        if (x.type === "password") {
          x.type = "text";
          setHidden(false)
        } else {
          x.type = "password";
          setHidden(true)
        }
      }
    
    return (
        <> 
            <div className="c-passwordEye">
                {props.children}
                <span className={"c-passwordEye__icon " + (hidden ? "icon-eye" : "icon-eye-off")} onClick={()=>myFunction()}></span>
            </div>
        </>
    )
}