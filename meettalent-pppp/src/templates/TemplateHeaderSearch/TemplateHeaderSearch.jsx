import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import {CSSTransition} from 'react-transition-group';

import { PageContext } from '../../shared/contexts/PageContext';
import { OffersContext } from '../../shared/contexts/OffersContext';
import { SearchFilterContext } from '../../shared/contexts/SearchFilterContext';
import { API } from '../../shared/services/api';

import { Header } from '../../shared/components/Header/Header';
import { SearchBar } from '../../shared/components/SearchBar/SearchBar';
import { getCookieUtil } from '../../shared/utils/getCookieUtil';

export function TemplateHeaderSearch({children, theme, center, title, backHistory, headerImg , opacity, mainRound, userAndChat}){

    const [page, setPage] = useState(1);
    
    const [offers, setOffers] = useState([]);

    const [toFilter, setToFilter] = useState();
    const [filtered, setFiltered] = useState();

    const history = useHistory();

    const stringUser = getCookieUtil('user');
    const user = JSON.parse(stringUser ? stringUser : '{}');
    const userId = user.id;

    const getOffers = () => {
        if(userId){
            API.get('http://localhost:5500/offer/'+userId).then(res => {
                setOffers(res.data.results);
            });
        }
    }

    const pageBack = () => {
        if(page === 1){
            history.push(backHistory);
        }else{
            setPage(page - 1);
        }  
    }

    useEffect(getOffers, []);

    return(
        <PageContext.Provider value={{ page, setPage }}>
        <OffersContext.Provider value={{ offers, setOffers }}>
            <div className={'b-'+theme+'-theme'}>

                {!headerImg &&
                    <CSSTransition
                        appear
                        in
                        classNames='header-transition'
                        timeout={800}
                    >
                        <Header back={true} title={title} leftFn={pageBack}/>
                    </CSSTransition>
                }

                {headerImg &&
                    <CSSTransition
                        appear
                        in
                        classNames='header-transition'
                        timeout={800}
                    >
                        <Header headerImg={true} opacity={opacity ? true : false}/>
                    </CSSTransition>
                }
                <SearchBar toFilter={toFilter} setFiltered={setFiltered} userAndChat={userAndChat ? true : false}/>

                <SearchFilterContext.Provider value={{setToFilter, filtered}}>
                    <main className={center ? (!mainRound ? "b-main b-main--wSearch b-main--center" : "b-main b-main--wSearch b-main--round b-main--center") : (!mainRound ? "b-main b-main--wSearch" : "b-main b-main--wSearch b-main--round")}>
                        {children}
                    </main>
                </SearchFilterContext.Provider>
            </div>
        </OffersContext.Provider>
        </PageContext.Provider>
        
    )
}