import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import {CSSTransition} from 'react-transition-group';

import { PageContext } from '../../shared/contexts/PageContext';
import { OffersContext } from '../../shared/contexts/OffersContext';
import { CandidatesContext } from '../../shared/contexts/CandidatesContext';
import { API } from '../../shared/services/api';

import { Header } from '../../shared/components/Header/Header';

import { getCookieUtil } from '../../shared/utils/getCookieUtil';

export function TemplateHeader({children, theme, center, title, setTitle,  backHistory, headerImg, opacity, mainRound}){

    const [page, setPage] = useState(1);

    const [offers, setOffers] = useState([]);
    const [candidates, setCandidates] = useState([]);

    const [showClose, setShowClose] = useState(false);

    const history = useHistory();

    const stringUser = getCookieUtil('user');
    const user = JSON.parse(stringUser ? stringUser : '{}');
    const userId = user.id;

    const getOffers = () => {
        if(userId){
            API.get('http://localhost:5500/offer/'+userId).then(res => {
                setOffers(res.data.results);
            });
        }
    }

    const getCandidates = () => {
        if(userId){
            API.get('http://localhost:5500/candidates/').then(res => {
                setCandidates(res.data.results);
            });
        }
    }

    const pageBack = () => {
        if(page === 1){
            history.push(backHistory);
        }else{
            setPage(page - 1);
        }  
    }

    const closePage = () => {
        history.push(backHistory);
        setShowClose(false);
    }

    useEffect(getOffers, []);
    useEffect(getCandidates, []);

    return(
        <PageContext.Provider value={{ page, setPage, setShowClose, setTitle }}>
        <OffersContext.Provider value={{ offers, setOffers }}>
        <CandidatesContext.Provider value={{ candidates, setCandidates }}>
            <div className={'b-'+theme+'-theme'}>
                {!headerImg &&
                    <CSSTransition
                        appear
                        in
                        classNames='header-transition'
                        timeout={800}
                    >
                        <Header back={true} title={title} leftFn={pageBack} rightFn={closePage} showClose={showClose}/>
                    </CSSTransition>
                }

                {headerImg &&
                    <CSSTransition
                        appear
                        in
                        classNames='header-transition'
                        timeout={800}
                    >
                        <Header headerImg={true} opacity={opacity ? true : false}/>
                    </CSSTransition>
                }

                <main className={center ? (!mainRound ? "b-main b-main--center" : "b-main b-main--round b-main--center") : (!mainRound ? "b-main" : "b-main b-main--round")}>
                    {children}
                </main>
            </div>
        </CandidatesContext.Provider>
        </OffersContext.Provider>
        </PageContext.Provider>
        
    )
}