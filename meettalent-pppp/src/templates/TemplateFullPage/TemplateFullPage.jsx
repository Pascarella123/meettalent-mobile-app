import React from 'react';

export function TemplateFullPage({ children, theme, contHistory, popUpText, popUpTitle, textBtn, inOffers }){

    return(
        <div className={'b-'+theme+'-theme'}>
            <main className="b-main b-main--center">
                {React.cloneElement(children, { contHistory, popUpText, popUpTitle, textBtn, inOffers })}
            </main>
        </div>    
    )
}